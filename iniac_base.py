import xml.etree.ElementTree
import os
import logging

import signal
import sqlite3 as lite


class IniacBase:
    conf_file_name = "/home/pi/iniac/config.xml"

    # connector and cursor of status table
    conn_status = None
    curs_status = None
    # connector and cursor of tmp table
    conn_tmp = None
    curs_tmp = None
    # connector and cursor of cmd table
    conn_cmd = None
    curs_cmd = None

    def __init__(self):
        log_level = int(self.get_param_from_xml("LOG_LEVEL"))
        log_file_name = self.get_param_from_xml("LOG_FILE_NAME")
        logging.basicConfig(level=log_level, filename=log_file_name,
                            format='%(asctime)s: %(message)s', datefmt='%m-%d-%Y %H:%M:%S')

        tmp_db_name = self.get_param_from_xml("TMP_DB_FILE")
        self.conn_tmp = lite.connect(tmp_db_name)
        self.curs_tmp = self.conn_tmp.cursor()
        os.system('chmod 777 ' + tmp_db_name)     # change permission of tmp db file
        self.check_tmp_table()

        status_db_name = self.get_param_from_xml("STATUS_DB_FILE")
        self.conn_status = lite.connect(status_db_name)
        self.curs_status = self.conn_status.cursor()
        os.system('chmod 777 ' + status_db_name)     # change permission of status db file
        self.check_status_table()

        cmd_db_name = self.get_param_from_xml("CMD_DB_FILE")
        self.conn_cmd = lite.connect(cmd_db_name)
        self.curs_cmd = self.conn_cmd.cursor()
        os.system('chmod 777 ' + cmd_db_name)     # change permission of status db file
        self.check_cmd_table()

    def set_config_param(self, tag_name, new_val):
        """
        set new parameter to config.xml file
        :param tag_name:    Name of new tag
        :param new_val:     new value
        :return:
        """
        et = xml.etree.ElementTree.parse(self.conf_file_name)

        for child_of_root in et.getroot():
            if child_of_root.tag == tag_name:
                child_of_root.text = new_val
                et.write(self.conf_file_name)
                return True
        return False

    # check DB whether status table exists or not
    # if not, create status table
    def check_status_table(self):
        """
        No. sn, dev_info, dev_name, sample_rate, temp, alive
        """
        try:
            sql = 'create table if not exists status ' \
                  '(' \
                  'id INTEGER PRIMARY KEY AUTOINCREMENT, ' \
                  'serial_num TEXT ,' \
                  'dev_info TEXT ,' \
                  'dev_name TEXT ,' \
                  'rate NUMERIC ,' \
                  'alive TEXT, ' \
                  'thr_min NUMERIC , ' \
                  'thr_max NUMERIC , ' \
                  'last_time TEXT ' \
                  ');'
            self.curs_status.execute(sql)
            self.conn_status.commit()
            return True

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            self.add_log('error', "Creating Table failed...")

    def check_cmd_table(self):
        """
        check whether cmd_table exists and if not, create it
        :return:
        """
        try:
            sql = 'create table if not exists table_cmd (id INTEGER PRIMARY KEY AUTOINCREMENT, cmd NUMERIC); '
            self.curs_cmd.execute(sql)
            self.conn_cmd.commit()
            return True
        except lite.Error as e:
            print("Error %s:" % e.args[0])
            return None

    def check_tmp_table(self):
        """
        check DB whether tmp table exists or not...    if not, create tmp table
        No. sn, dev_info, dev_name, sample_rate, temp, alive
        """

        try:
            sql = 'create table if not exists tmp ' \
                  '(' \
                  'id INTEGER PRIMARY KEY AUTOINCREMENT, ' \
                  'dev_id TEXT ,' \
                  'status TEXT ,' \
                  'last_val TEXT ,' \
                  'last_time TEXT, ' \
                  'mtc_temp TEXT' \
                  ');'

            self.curs_tmp.execute(sql)
            self.conn_tmp.commit()
    #        logging.info("Succeed to create 'tmp' table.")
            return True

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            logging.error("Creating Table failed...")

    def get_param_from_xml(self, param):
        """
        Get configuration parameters from the config.xml
        :param param: parameter name
        :return: if not exists, return None
        """
        root = xml.etree.ElementTree.parse(self.conf_file_name).getroot()
        tmp = None
        for child_of_root in root:
            if child_of_root.tag == param:
                tmp = child_of_root.text
                break
        return tmp

    def add_log(self, level, content):
        ret = True
        if level == 'debug':
            logging.debug(content)
        elif level == 'info':
            logging.info(content)
        elif level == 'warn':
            logging.warn(content)
        elif level == 'error':
            logging.error(content)
        elif level == 'fatal':
            logging.fatal(content)
        else:
            ret = False
        return ret

    @staticmethod
    def kill_script(sn):
        proc_name = "read_sensor_data"
        cmd_result = os.popen("ps ax | grep " + proc_name + " | grep -v grep")

        for line in cmd_result:
            fields = line.split()
            pid = fields[0]         # pid is the first value
            fields.reverse()        # the param of python script is the last one,so we reverse this
            print("PID : " + pid + ", param : " + fields[0])
            if sn == fields[0]:
                os.kill(int(pid), signal.SIGKILL)	# kill 1st process(old)
        return True