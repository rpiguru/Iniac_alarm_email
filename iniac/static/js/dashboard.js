

//DataTables
//Sort file size data.
jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "file-size-units": {
        K: 1024,
        M: Math.pow(1024, 2),
        G: Math.pow(1024, 3),
        T: Math.pow(1024, 4),
        P: Math.pow(1024, 5),
        E: Math.pow(1024, 6)
    },

    "file-size-pre": function (a) {
        var x = a.substring(0, a.length - 1);
        var x_unit = a.substring(a.length - 1, a.length);
        if (jQuery.fn.dataTableExt.oSort['file-size-units'][x_unit]) {
            return parseInt(x * jQuery.fn.dataTableExt.oSort['file-size-units'][x_unit], 10);
        }
        else {
            return parseInt(x + x_unit, 10);
        }
    },

    "file-size-asc": function (a, b) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "file-size-desc": function (a, b) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
});

//DataTables
//Sort numeric data which has a percent sign with it.
jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "percent-pre": function (a) {
        var x = (a === "-") ? 0 : a.replace(/%/, "");
        return parseFloat(x);
    },

    "percent-asc": function (a, b) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "percent-desc": function (a, b) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
});

//DataTables
//Sort IP addresses
jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "ip-address-pre": function (a) {
        // split the address into octets
        //
        var x = a.split('.');

        // pad each of the octets to three digits in length
        //
        function zeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return new Array(+(zero > 0 && zero)).join("0") + num;
        }

        // build the resulting IP
        var r = '';
        for (var i = 0; i < x.length; i++)
            r = r + zeroPad(x[i], 3);

        // return the formatted IP address
        //
        return r;
    },

    "ip-address-asc": function (a, b) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "ip-address-desc": function (a, b) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
});

// If dataTable with provided ID exists, destroy it.
function destroy_dataTable(table_id) {
    var table = $("#" + table_id);
    var ex = document.getElementById(table_id);
    if ($.fn.DataTable.fnIsDataTable(ex)) {
        table.hide().dataTable().fnClearTable();
        table.dataTable().fnDestroy();
    }
}

function get_os_data(url, element) {
    $.get(url, function (data) {
        $(element).text(data);
    }, "json");
}

var dashboard = {};

dashboard.getUptime = function () {
    get_os_data('/info/uptime/', "#get-uptime");
};

dashboard.getOSname = function () {
    get_os_data('/info/platform/osname/', "#get-osname");
};

dashboard.getHostname = function () {
    get_os_data('/info/platform/hostname/', "#get-hostname");
};

dashboard.getKernel = function () {
    get_os_data('/info/platform/kernel/', "#get-kernel");
};

dashboard.getLocalIP = function () {
    get_os_data('/info/platform/localip/', "#get-localip");
};

dashboard.getLocation = function () {
    $.get('/info/platform/location/', function (data) {
        if (data.substring(0, 3) == 'Not'){
            $('#txt_loc').val(data);
            $('#get-location2').text(data);
        }
        else{
            // received data is base64 type, so decode it
            // replace '_' to '='
            var val = data.replace(/\_/g,'=');
            $('#txt_loc').val(atob(val));
            $('#get-location2').text(atob(val));
        }
    }, "json");
};

dashboard.getCPUcount = function () {
    get_os_data('/info/getcpus/count/', "#get-cpucount");
};

dashboard.getCPUtype = function () {
    get_os_data('/info/getcpus/type/', "#get-cputype");
};

var global = {};
var dev_info = new Array();
var dev_name = new Array();
var id_list = new Array();
var reading_rate = '';
var min_list = new Array();
var max_list = new Array();

global.dev_info_list = {
    sensor_list : dev_info
};


var formatTime = function(val) {
    var value = Number(val);

    var minutes = parseInt(value/60);
    var seconds = parseInt(value%60);

    // the above dt.get...() functions return a single digit
    // so I prepend the zero here when needed
    if (minutes < 10)
     minutes = '0' + minutes;

    if (seconds < 10)
     seconds = '0' + seconds;

    return minutes + ":" + seconds;
};


dashboard.getDisk = function () {
    $.getJSON('/info/getdisk/', function (data) {
        // console.log('DATA: ');
        // console.log(data);
        var $filterPs = $("#filter-ps");
        $filterPs.val("").off("keyup");
        var table = $("#get_disk");

        //save status of checklist before removing whole table
        var check_list = new Array();
        for (k = 0; k < data.length; k++) {
            check_list[k] = $('input:checkbox[id="check_' + k.toString()+'"]').is(':checked');
            min_list[k] = $('#txt_min_' + k.toString()).val();
            max_list[k] = $('#txt_max_' + k.toString()).val();
        }

        table.empty();

        dev_info = [];      // initialize the list of device info
        dev_name = [];
        id_list = [];
        table.append('<table style="cursor:hand;">');

        // create table header
        var th = '<tr><th>No.</th><th>Graph</th><th>Sensor</th>' +
            '<th>Last Value</th><th>Temp ℃</th>' +
            '<th>Last time</th><th>Thrs Min</th><th>Thrs Max</th><th>Setup</th></tr>';
        table.append(th);

        // Insert rows
        $.each(data, function(i) {
            id_list[i] = data[i][0];
            var row = '<tr id="row_' + i.toString() + '" style="cursor:hand">';
            var device_name = data[i][3];
            var device_type = data[i][2];
            row = row + '<td align="center">' + (i+1).toString() + '</td>';      // insert No.
            row = row + '<td><input type="checkbox" id="check_' + i.toString() + '"></td>'; // insert checkbox
            // insert icon and Dev type
            row = row + '<td class="dev_type"><img src="../static/img/icon/' + device_type + '_1.png" width="20px"> ' +
                '<span id = "span_' + i.toString() + '"></span></td>';

            // insert value
            var last_value = data[i][6];
            switch (device_name[0]){
                case 'O':
                    last_value += ' mV';
                    break;
                case 'D':
                    last_value += ' Mg/L';
                    break;
                case 'E':
                    last_value += ' uS';
                    break;
                case 'F':
                    last_value += ' lpm';
                    break;
                case 'R':
                    last_value += ' ℃';
                    break;
                default:
                    break;
            }
            row = row + '<td>' + last_value + '</td>';

            row = row + '<td>' + data[i][5] + '</td>';  // insert temp

            // insert timestamp
            var timestamp = data[i][7].split(' ');

            row = row + '<td>' + timestamp[1] + '</td>';

            var str_min = '<td><input type="text" style="width: 30px" id="txt_min_' + data[i][0] +
                '" value="' + data[i][8] + '">'  + '<input type="button" onclick="set_min(' + data[i][0] + ')" value="Set"></td>';
            row = row + str_min;

            var str_max = '<td><input type="text" style="width: 30px" id="txt_max_' + data[i][0] +
                '" value="' + data[i][9] + '">'  + '<input type="button" onclick="set_max(' + data[i][0] + ')" value="Set"></td>';
            min_list[i] = data[i][8];
            max_list[i] = data[i][9];
            row = row + str_max;

            row = row + '<td><a href="../setup/' + device_type + '-' + id_list[i] + '" style="color:white;background:black">' +
                        'setup </a></td>';  // insert setup button

            row = row + '</tr>';
            table.append(row);
            dev_info[i] = data[i][2];
            dev_name[i] = device_name;

        });
        table.append('</table>');

        var rate = data[0][4];      // get the sampling rate in sec
        var formattedTime = formatTime(rate);

        if (reading_rate == ''){
            var slider = $("#slider_rate").data("ionRangeSlider");
            var var_option = slider.options;
            var cur_num = 0;
            for (i=0; i < var_option.values.length; i++){
                var tmp = var_option.values[i];
                if (var_option.values[i] == formattedTime){
                    cur_num = i;
                    break;
                }
            }
            slider.update({
                from: cur_num
            });
            reading_rate = formattedTime;
        }

        $('#title_rate').html('   ' + formattedTime);

        // set check list as previous values
        for (k = 0; k < data.length; k++) {
            $('input:checkbox[id="check_' + k.toString()+'"]').attr("checked", check_list[k]);
        }

        // insert background and color and info
        for (i = 0; i < data.length; i ++) {
            var row_obj = $('#row_' + i.toString());
            var info_obj = $('#span_' + i.toString());
            switch (dev_name[i][0]) {
                case 'p' :  // pH
                    row_obj.css({"background": "#F05133", "color": '#FFFFFF'});
                    info_obj.html(dev_name[i]);
                    break;
                case 'D' :  // DO
                    row_obj.css({"background": '#FFD200', "color": '#000000'});
                    info_obj.html(dev_name[i].replace('DO','Dissolved Oxygen'));
                    break;
                case 'O' :  // OR
                    row_obj.css({"background": '#00C0F3', "color": '#FFFFFF'});
                    info_obj.html(dev_name[i]);
                    break;
                case 'E' :  // EC
                    row_obj.css({"background": '#28903B', "color": '#FFFFFF'});
                    info_obj.html(dev_name[i].replace('EC', 'Conductivity'));
                    break;
                case 'F' :  // FLO
                    row_obj.css({"background": '#7AC4D3', "color": '#FFFFFF'});
                    info_obj.html(dev_name[i].replace('FLO', 'Flow'));
                    break;
                case 'R' :  // Temperature
                    row_obj.css({"background": '#756e6e' , "color": '#FFFFFF'});
                    info_obj.html(dev_name[i].replace('RTD', 'Temp'));
                    break;
                default  :
                    break;
            }
        }
    });
};

function set_min(i) {
    var val = $('#txt_min_' + i.toString()).val();
    var url = '/info/set_min/' + i + "_" + val;
    if ($.isNumeric(val)){
        $.getJSON(url, function (data) {
        });
    }
}

function set_max(i) {
    var val = $('#txt_max_' + i.toString()).val();
    var url = '/info/set_max/' + i + "_" + val;
    if ($.isNumeric(val)){
        $.getJSON(url, function (data) {
        });
    }
}

dashboard.getUsers = function () {
    $.getJSON('/info/getusers/', function (data) {
        destroy_dataTable("get_users");
        var $filterPs = $("#filter-ps");
        $filterPs.val("").off("keyup");
        var psTable = $("#get_users").dataTable({
            aaData: data,
            aoColumns: [
                { sTitle: "USER" },
                { sTitle: "TTY" },
                { sTitle: "LOOGED IN FROM",
                    sDefaultContent: "unavailable" }
            ],
            aaSorting: [
                [0, "desc"]
            ],
            bPaginate: true,
            sPaginationType: "two_button",
            bFilter: false,
            bAutoWidth: false,
            bInfo: false
        }).fadeIn();
        $filterPs.on("keyup", function () {
            psTable.fnFilter(this.value);
        });
    });
};

function progress(percent, $element) {
    var progressBarWidth = percent * $element.width() / 100;
    $element.find('div').animate({ width: progressBarWidth }, 500).html(percent + "% ");
}

dashboard.getNetstat = function () {
    $.getJSON('/info/getnetstat/', function (data) {
        var log_count = 0;
        for (i=0; i < data.length; i++){
            log_count = log_count + Number(data[i]);
        }
        var str_count = log_count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $('#mem_size').html(str_count);
        log_count = log_count/100000;
        if (log_count >= 100){
            log_count = 100;
        }
        progress(log_count, $('#progressBar'));

    });
};

dashboard.getAlarmInfo = function () {
    $.getJSON('/info/get_alarm_int/', function (data) {
        $('#alarm_int').val(data);
    });
    $.getJSON('/info/get_alarm_email/', function (data) {
        $('#alarm_email').val(data);
    });
};



dashboard.getProc = function () {
    $.getJSON('/info/proc/', function (data) {
        destroy_dataTable("get_proc");
        var $filterPs = $("#filter-ps");
        $filterPs.val("").off("keyup");
        var psTable = $("#get_proc").dataTable({
            aaData: data,
            aoColumns: [
                { sTitle: "USER" },
                { sTitle: "PID" },
                { sTitle: "%CPU" },
                { sTitle: "%MEM" },
                { sTitle: "VSZ" },
                { sTitle: "RSS" },
                { sTitle: "TTY" },
                { sTitle: "STAT" },
                { sTitle: "START" },
                { sTitle: "TIME" },
                { sTitle: "COMMAND" }
            ],
            bPaginate: true,
            sPaginationType: "full_numbers",
            bFilter: true,
            sDom: "lrtip",
            bAutoWidth: false,
            bInfo: false
        }).fadeIn();
        $filterPs.on("keyup", function () {
            psTable.fnFilter(this.value);
        });
    });
};

dashboard.getLastValue = function(sn) {
    $.getJSON('/info/getlastval/' + sn, function (data) {
        var tmp = data.split(',');
        if (tmp.length == 1){
            $('#last_val').text(data);
        }
        else{
            $('#last_val_0').text(tmp[0]);
            $('#last_val_1').text(tmp[1]);
            $('#last_val_2').text(tmp[2]);
        }
    });
};

dashboard.set_mtc_temp = function(sn) {
    $.getJSON('/info/getmtctemp/' + sn, function (data) {
        var tmp = data.split('_');
        $('#input_temp').val(tmp[1]);
        if (data[0] == 'm'){
            $('#radio_mtc').prop("checked", true);
        }
    });
};

dashboard.setDateTime = function() {
    $.getJSON('/info/getdatetime/', function (data) {
        var start_date = data.split(' ');
        $.datetimepicker.setLocale('en');
        $('#datetimepicker').datetimepicker({
            format:'m/d/Y H:i',
            dayOfWeekStart : 1,
            lang:'en',
            theme:'dark',
            value:data,
            step:10,
            startDate:	start_date[0]
        });
    });
};

dashboard.getIps = function () {

};

// Expand-Contract div/table
$(document).ready(function () {
    $(".widget-content").show();
    $(".widget-header").click(function () {
        $(this).next(".widget-content").slideToggle(500);
        $("i", this).toggleClass("icon-minus icon-plus");
    });
});
