
import platform
import os, logging, sys
import multiprocessing
from datetime import timedelta
from iniac.settings import BASE_PATH
import socket, fcntl, struct

sys.path.append(BASE_PATH)


# from geoip import geolite2
# import ipgetter

import urllib2, json
import xlsxwriter
import sqlite3 as lite
import datetime
import time

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.http import HttpResponse

from iniac.settings import TIME_JS_REFRESH, TIME_JS_REFRESH_LONG, TIME_JS_REFRESH_NET, VERSION

time_refresh = TIME_JS_REFRESH
time_refresh_long = TIME_JS_REFRESH_LONG
time_refresh_net = TIME_JS_REFRESH_NET
version = VERSION


status_db = BASE_PATH + "data/status.sqlite"
tmp_db = BASE_PATH + "data/tmp.sqlite"
cmd_db = BASE_PATH + "data/cmd.sqlite"

# logger = logging.getLogger('django')
log_level = 10
log_file_name = BASE_PATH + "data/iniac.log"
logging.basicConfig(level=log_level, filename=log_file_name,
                    format='%(asctime)s: %(message)s', datefmt='%m-%d-%Y %H:%M:%S')


@login_required(login_url='/login/')
def index(request):
    """

    Index page.

    """
    return HttpResponseRedirect('/main')


def chunks(get, n):
    return [get[i:i + n] for i in range(0, len(get), n)]


def get_uptime():
    """
    Get uptime
    """
    try:
        with open('/proc/uptime', 'r') as f:
            uptime_seconds = float(f.readline().split()[0])
            uptime_time = str(timedelta(seconds=uptime_seconds))
            data = uptime_time.split('.', 1)[0]

    except Exception as err:
        data = str(err)

    return data


def get_info_list():

    return True


def add_num(in_list):       # add sequence number at the end of string

    list_info = []
    for i in range(len(in_list)):
        list_tmp = in_list[:(i+1)]
        count = list_tmp.count(in_list[i])
        if count > 1:
            tt = in_list[i] + '(' + str(count - 1) + ')'
            list_info.append(tt)
        else:
            list_info.append(in_list[i])

    return list_info


def get_ipaddress():
    """
    Get the list of device info to display at the bottom of chart
    """
    conn = None

    data = []
    try:
        conn = lite.connect(status_db)
        curs = conn.cursor()
        query = "SELECT dev_name FROM status WHERE alive = 'on';"
        curs.execute(query)
        rows = curs.fetchall()
        conn.close()
        data = list(rows)

        return data

    except lite.Error as e:
        print("Error %s:" % e.args[0])
        logging.error("Getting active device name failed...")

    finally:
        if conn:
            conn.close()


def get_cpus():
    """
    Get the number of CPUs and model/type
    """
    try:
        pipe = os.popen("cat /proc/cpuinfo |" + "grep 'model name'")
        data = pipe.read().strip().split(':')[-1]
        pipe.close()

        if not data:
            pipe = os.popen("cat /proc/cpuinfo |" + "grep 'Processor'")
            data = pipe.read().strip().split(':')[-1]
            pipe.close()

        cpus = multiprocessing.cpu_count()

        data = {'cpus': cpus, 'type': data}

    except Exception as err:
        data = str(err)

    return data


def get_users():
    """
    Get the current logged in users
    """
    try:
        pipe = os.popen("who |" + "awk '{print $1, $2, $6}'")
        data = pipe.read().strip().split('\n')
        pipe.close()

        if data == [""]:
            data = None
        else:
            data = [i.split(None, 3) for i in data]

    except Exception as err:
        data = str(err)

    return data


def get_traffic(cnt):
    """
    Get the latest values of sensors
    :param cnt: count of last values
    :return:
    """
    conn = None
    try:
        conn = lite.connect(status_db)
        curs = conn.cursor()
        query = "SELECT dev_name FROM status WHERE alive='on' or alive='cal';"
        curs.execute(query)
        rows = curs.fetchall()
        conn.close()

        dev_list = []
        for dd in rows:
            dev_list.append(dd[0])

        # sort list - move RTD to the first
        for i in range(len(dev_list)):
            if dev_list[i][0] == 'R':
                dev_list.insert(0, dev_list.pop(i))

        data_time = []
        db_tmp = BASE_PATH + 'data/' + dev_list[0] + '.sqlite'
        con_dev = lite.connect(db_tmp)
        curs_tmp = con_dev.cursor()
        # get last timestamps
        sql = "select timestamp from '" + dev_list[0] + "' ORDER BY id DESC LIMIT " + cnt + " ;"

        curs_tmp.execute(sql)
        val_time = curs_tmp.fetchall()

        for t in val_time:
            tmp = t[0].split(' ')
            tt = tmp[1]
            data_time.append(tt[:-3])       # display only hour/minute
        con_dev.close()

        data = []
        data_time.reverse()

        data.append(data_time)
        for dev in dev_list:
            data_val = []
            db_tmp = BASE_PATH + 'data/' + dev + '.sqlite'
            con_dev = lite.connect(db_tmp)
            curs_tmp = con_dev.cursor()
            sql = "select val1 from '" + dev + "' ORDER BY id DESC LIMIT " + cnt + ";"       # get last values
            curs_tmp.execute(sql)
            val_sensor = curs_tmp.fetchall()
            for d in val_sensor:
                data_val.append(d[0])
            con_dev.close()
            data_val.reverse()
            data.append(data_val)

        return data

    except lite.Error as e:
        logging.error("Getting last values from DB failed..." % e)

    finally:
        if conn:
            conn.close()


def getplace(lat, lon):

    url = "http://maps.googleapis.com/maps/api/geocode/json?"
    url += "latlng=%s,%s&sensor=false" % (lat, lon)
    v = urllib2.urlopen(url)
#    str_response = v.readall().decode('utf-8')
    j = json.load(v)

    components = j['results'][0]['address_components']
    sublocal = city = ""

    for c in components:
        if "sublocality" in c['types']:
            sublocal = c['long_name']
        if "administrative_area_level_1" in c['types']:
            city = c['short_name']

    result = sublocal + ", " + city
    return result


def delete_all(dev):
    db_tmp = BASE_PATH + 'data/' + dev + '.sqlite'

    con_dev = lite.connect(db_tmp)
    curs_tmp = con_dev.cursor()
    sql = "delete from '" + dev + "';"
    curs_tmp.execute(sql)
    con_dev.commit()
    con_dev.close()
    return True


def clear_all():
    """
    Get the latest values of sensors
    """
    conn = None

    try:
        conn = lite.connect(status_db)
        curs = conn.cursor()
        query = "SELECT dev_name FROM status"
        curs.execute(query)
        rows = curs.fetchall()
        conn.close()

        dev_list = []
        for dd in rows:
            tt = list(dd)
            dev_list.append(tt[0])

#        delete_all(sn_list[5])
        for dev in dev_list:
            if dev != 'location':
                delete_all(dev)

        return True

    except lite.Error as e:
        logging.info("Error %s:" % e.args[0])
        logging.error("Deleting DB failed...")
        return False
    finally:
        if conn:
            conn.close()

# def get_location():
#     myip = ipgetter.myip()                      # get my external IP
#     match = geolite2.lookup(myip)               # get GEO information from my external IP
#     geo_info = match.location
#     loc = getplace(geo_info[0], geo_info[1])    # get the City name and Country name
#     return loc

def get_local_ip(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        addr = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])
    except IOError:
        addr = None
    return addr

def get_platform():
    """
    Get the OS name, hostname and kernel,location
    """
    try:
        osname = " ".join(platform.linux_distribution())
        uname = platform.uname()

        if osname == '  ':
            osname = uname[0]

        localip = get_local_ip("wlan0")
        if not localip:
            localip = get_local_ip("eth0")

        data = {'osname': osname, 'hostname': uname[1], 'kernel': uname[2], 'localip':localip}

    except Exception as err:
        data = str(err)

    return data


def get_disk():
    """
    get detailed list of active devices from status table
    :return:
    """

    conn = None

    try:
        conn = lite.connect(status_db)
        curs = conn.cursor()
        query = "SELECT * FROM status WHERE alive = 'on' or alive = 'cal';"
        curs.execute(query)
        rows = curs.fetchall()
        conn.close()

        dev_list = []
        for dd in rows:
            dev_list.append(list(dd))

        # sort list - move RTD to the first
        for i in range(len(dev_list)):
            if dev_list[i][3][0] == 'R':   # the 4th value is dev_name
                dev_list.insert(0, dev_list.pop(i))

        data = []
        for dev in dev_list:
            # data type is status is id, serial_num, dev_info, dev_name, rate, alive
            data_val = dev[:5]

            dev_name = data_val[3]     # dev_name is the 4th value
            db_tmp = BASE_PATH + 'data/' + str(dev_name) + '.sqlite'

            con_dev = lite.connect(db_tmp)
            curs_tmp = con_dev.cursor()

            sql = "select * from '" + dev_name + "' ORDER BY id DESC LIMIT 1;"       # get last one
            curs_tmp.execute(sql)
            val_sensor = curs_tmp.fetchone()
            if val_sensor:
                # data type of table is id, timestamp, val1, val2, val3, val4, temp
                data_val.append(val_sensor[6])      # temperature
                data_val.append(val_sensor[2])      # last value
                # insert time
                last_time = datetime.datetime.strptime(val_sensor[1], "%Y-%m-%d %H:%M:%S")
                str_time = last_time.strftime("%m/%d/%Y %H:%M:%S")
                data_val.append(str_time)
            else:   # if there is no values on table
                data_val.append('N/A')      # temperature
                data_val.append('N/A')      # last value
                data_val.append('N/A')      # time

            data_val.append(dev[6])     # add thr_min
            data_val.append(dev[7])     # add thr_max

            con_dev.close()
            data.append(data_val)

        return data

    except lite.Error as e:
        logging.error("Getting active devices failed...")

    finally:
        if conn:
            conn.close()


def get_dev_name_by_id(_id):
    """
    get device name with given id
    :param _id:
    :return:
    """
    conn = None
    try:
        conn = lite.connect(status_db)
        curs = conn.cursor()
        query = "SELECT dev_name FROM status WHERE id=" + _id + ";"
        curs.execute(query)
        rows = curs.fetchone()
        conn.close()

        if rows:
            return rows[0]
        else:
            return None

    except lite.Error as e:
        logging.error("Getting device name by sn failed...")
        return None
    finally:
        if conn:
            conn.close()


def set_value(request):
    """
    get setting request from the client and register it on cmd_table
    :param request:
    :return:
    """

    if not request:
        return False
    # insert request to cmd_table

    conn = None

    try:
        conn = lite.connect(cmd_db)
        curs = conn.cursor()
        curs.execute("insert into table_cmd (cmd) values('" + request + "');")
        conn.commit()
        conn.close()
        return True
    except lite.Error as e:
        print("Error %s:" % e.args[0])
        logging.error("registering request to db failed...")
        return False
    finally:
        if conn:
            conn.close()

def get_disk_rw():
    """
    Get the disk reads and writes
    """
    try:
        pipe = os.popen("cat /proc/partitions | grep -v 'major' | awk '{print $4}'")
        data = pipe.read().strip().split('\n')
        pipe.close()

        rws = []
        for i in data:
            if i.isalpha():
                pipe = os.popen("cat /proc/diskstats | grep -w '" + i + "'|awk '{print $4, $8}'")
                rw = pipe.read().strip().split()
                pipe.close()

                rws.append([i, rw[0], rw[1]])

        if not rws:
            pipe = os.popen("cat /proc/diskstats | grep -w '" + data[0] + "'|awk '{print $4, $8}'")
            rw = pipe.read().strip().split()
            pipe.close()

            rws.append([data[0], rw[0], rw[1]])

        data = rws

    except Exception as err:
        data = str(err)

    return data


def get_mem():
    """
    Get memory usage
    """
    try:
        pipe = os.popen(
            "free -tmo | " + "grep 'Mem' | " + "awk '{print $2,$4,$6,$7}'")
        data = pipe.read().strip().split()
        pipe.close()

        allmem = int(data[0])
        freemem = int(data[1])
        buffers = int(data[2])
        cachedmem = int(data[3])

        # Memory in buffers + cached is actually available, so we count it
        # as free. See http://www.linuxatemyram.com/ for details
        freemem += buffers + cachedmem

        percent = (100 - ((freemem * 100) / allmem))
        usage = (allmem - freemem)

        mem_usage = {'usage': usage, 'buffers': buffers, 'cached': cachedmem, 'free': freemem, 'percent': percent}

        data = mem_usage

    except Exception as err:
        data = str(err)

    return data


def get_cpu_usage():
    """
    Get the CPU usage and running processes
    """
    try:
        pipe = os.popen("ps aux --sort -%cpu,-rss")
        data = pipe.read().strip().split('\n')
        pipe.close()

        usage = [i.split(None, 10) for i in data]
        del usage[0]

        total_usage = []

        for element in usage:
            usage_cpu = element[2]
            total_usage.append(usage_cpu)

        total_usage = sum(float(i) for i in total_usage)

        total_free = ((100 * int(get_cpus()['cpus'])) - float(total_usage))

        cpu_used = {'free': total_free, 'used': float(total_usage), 'all': usage}

        data = cpu_used

    except Exception as err:
        data = str(err)

    return data


def get_load():
    """
    Get load average
    """
    try:
        data = os.getloadavg()[0]
    except Exception as err:
        data = str(err)

    return data


def get_netstat():
    """
    get total row count of all sensor's database
    :return:
    """
    conn = None

    try:
        conn = lite.connect(status_db)
        curs = conn.cursor()
        query = "SELECT dev_name FROM status where serial_num!='location';"
        curs.execute(query)
        rows = curs.fetchall()
        conn.close()

        dev_list = []
        for dd in rows:
            tmp = list(dd)
            dev_list.append(tmp[0])

        data = []
        for dev in dev_list:

            db_tmp = BASE_PATH + 'data/' + dev + '.sqlite'

            con_dev = lite.connect(db_tmp)
            curs_tmp = con_dev.cursor()

            sql = "select count(*) from '" + dev + "';"       # get count of rows
            curs_tmp.execute(sql)
            val_count_row = curs_tmp.fetchone()
            con_dev.close()
            data.append(val_count_row[0])

        return data

    except lite.Error as e:
        logging.error("Getting row count failed...")
    finally:
        if conn:
            conn.close()


def get_lastval(_id):
    """
    get last value from the tmp table and update last_time to now
    the watchdog script compares last_time value and determines whether sensor's setup page is still opened or not
    i.e. if time offset of last_time is greater than 8 sec, it decides that this sensor's setup page is closed
    see line 176 of iniac_watchdog.py

    the columns of tmp table is : id, dev_id, status, last_val, last_time, mtc_temp
    :param _id: id of sensor
    """
    con_dev = None
    try:
        con_dev = lite.connect(tmp_db)
        curs_tmp = con_dev.cursor()
        query = "select last_val from tmp where dev_id='" + _id + "';"
        curs_tmp.execute(query)
        last_val = curs_tmp.fetchone()

        str_time = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

        if last_val:
            query = "UPDATE tmp SET status='on', last_time='" + str_time + "' WHERE dev_id='" + _id + "';"
        else:
            query = "INSERT INTO tmp (dev_id, status, last_val, last_time, mtc_temp) VALUES ('" \
                    + _id + "','on','0', '" + str_time + "', 'atc');"
            last_val = [0]

    #    logging.info("QUERY : %s", query)

        curs_tmp.execute(query)
        con_dev.commit()
        return last_val[0]

    except lite.Error as e:
        logging.error("Getting last value from tmp table failed...")
        return 'N/A'
    finally:
        if con_dev:
            con_dev.close()


def get_mtc_temp(_id):
    """
    get mtc_temp from tmp table
    :param _id: device id of destination sensor
    """
    con_dev = None
    try:
        con_dev = lite.connect(tmp_db)
        curs_tmp = con_dev.cursor()
        query = "select mtc_temp from tmp where dev_id='" + _id + "';"
        curs_tmp.execute(query)
        last_val = curs_tmp.fetchone()
        return last_val[0]

    except lite.Error as e:
        print("Error %s:" % e.args[0])
        logging.error("Getting mtc_temp from tmp table failed...")
        return 'N/A'
    finally:
        if con_dev:
            con_dev.close()


@login_required(login_url='/login/')
def getall(request):
    return render_to_response('main.html', {'time_refresh': time_refresh,
                                            'time_refresh_long': time_refresh_long,
                                            'time_refresh_net': time_refresh_net,
                                            'version': version}, context_instance=RequestContext(request))


@login_required(login_url='/login/')
def setup_sensor(request, name):        # name is dev_type-sn

    dev_info = name.split('-')
    dev_id = dev_info[1]
    dev_name = get_dev_name_by_id(dev_id)

    is_rdt = False
    if dev_name[0] != 'R':     # if sensor is not RTD
        #  set temperature type
        # search RTD among active sensors, and if exists, set temp_type of this sensor as ATC and set its temp value
        rdt_dev = None
        try:
            conn = lite.connect(status_db)
            curs = conn.cursor()
            query = "SELECT * FROM status WHERE alive = 'on' or alive = 'cal';"
            curs.execute(query)
            rows = curs.fetchall()
            conn.close()

            dev_list = []
            for dd in rows:
                dev_list.append(list(dd))

            # get the last RTD... (since their sequence is reversed by sorting... see line 354 )
            for i in range(len(dev_list)):
                if dev_list[i][3][0] == 'R':  # the 4th value is dev_name
                    rdt_dev = dev_list[i][3]

        except lite.Error as e:
            logging.error("Finding RTD in status table failed...")

    #    rdt_dev = None     # for testing when RTD is not connected

        if rdt_dev:
            is_rdt = True
            # get last temperature value from table of RTD
            db_temp = BASE_PATH + 'data/' + rdt_dev + '.sqlite'

            con_temp = lite.connect(db_temp)
            curs_temp = con_temp.cursor()

            sql = "select val1 from '" + rdt_dev + "' ORDER BY id DESC LIMIT 1;"  # get last temperature
            curs_temp.execute(sql)
            val_sensor = curs_temp.fetchone()
            last_temp = str(val_sensor[0])
            con_temp.close()

    #        logging.info("LAST tempterature is %s" % last_temp)

            # update tmp table to show temperature values on Temperature field
            try:
                con_dev = lite.connect(tmp_db)
                curs_tmp = con_dev.cursor()
                query = "select last_val from tmp where dev_id='" + dev_id + "';"
                curs_tmp.execute(query)
                last_val = curs_tmp.fetchone()

                if last_val:    # if row of this sensor already exists, update
                    query = "UPDATE tmp SET mtc_temp='atc_" + last_temp + "' WHERE dev_id='" + dev_id + "';"
                else:           # otherwise insert
                    str_time = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
                    query = "INSERT INTO tmp (dev_id, status, last_val, last_time, mtc_temp) VALUES ('" \
                            + dev_id + "','on','0', '" + str_time + "', 'atc_" + last_temp + "');"
                curs_tmp.execute(query)
                con_dev.commit()
                con_dev.close()
            except lite.Error as e:
                logging.error("Updating mtc_temp as atc failed...")

    return render_to_response(dev_info[0] + '.html', {'dev_id': dev_id,
                                                      'dev_name': dev_name,
                                                      'rtd_exist': is_rdt,
                                                      'time_refresh_net': time_refresh_net},
                              context_instance=RequestContext(request))


