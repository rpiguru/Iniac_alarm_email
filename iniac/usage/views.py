
import json
import os, logging, time, sys
import xlsxwriter
import sqlite3 as lite
import datetime
from django.http import HttpResponse
from django.core.files import File

from main.views import *
from iniac.settings import TIME_JS_REFRESH, TIME_JS_REFRESH_LONG, TIME_JS_REFRESH_NET
import base64

from iniac.settings import BASE_PATH
import xml.etree.ElementTree

sys.path.append(BASE_PATH)

time_refresh = TIME_JS_REFRESH
time_refresh_long = TIME_JS_REFRESH_LONG
time_refresh_net = TIME_JS_REFRESH_NET


status_db = BASE_PATH + "data/status.sqlite"
tmp_db = BASE_PATH + "data/tmp.sqlite"

# logger = logging.getLogger('django')
log_level = 10
log_file_name = BASE_PATH + "data/iniac.log"
logging.basicConfig(level=log_level, filename=log_file_name,
                    format='%(asctime)s: %(message)s', datefmt='%m-%d-%Y %H:%M:%S')


@login_required(login_url='/login/')
def getnetstat(request):
    """
    Return netstat output
    """
    try:
        net_stat = get_netstat()
    except Exception:
        net_stat = None

    data = json.dumps(net_stat)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def clearall(request):
    """
    Return result of clearing all database
    """

    try:
        result = clear_all()
    except Exception:
        result = None

    data = json.dumps(result)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def setrate(request, name):

    conn = None
    try:
        conn = lite.connect(status_db)
        curs = conn.cursor()
        curs.execute("UPDATE status SET rate = " + name + " where serial_num != 'location';")
        conn.commit()
        conn.close()
        result = 'true'

    except lite.Error as e:
        print("Error %s:" % e.args[0])
        logging.error("Error %s:" % e.args[0])
        result = 'false'

    data = json.dumps(result)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def setlocation(request, name):

    conn = None
    try:
        conn = lite.connect(status_db)
        curs = conn.cursor()
        # see if exists
        curs.execute("select dev_info from status where serial_num='location';")
        conn.commit()
        rows = curs.fetchone()
        query = 'update status set dev_name="location", dev_info="' + name + '" where serial_num="location";'
        curs.execute(query)
        conn.commit()
        conn.close()
        result = 'true'

    except lite.Error as e:
        logging.error("Error %s:" % e.args[0])
        result = 'false'

    data = json.dumps(result)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def set_min(request, name):

    tmp = name.split('_')
    sensor_id = tmp[0]
    val = tmp[1]

    conn = None
    try:
        conn = lite.connect(status_db)
        curs = conn.cursor()
        query = 'update status set thr_min=' + val + ' where id=' + sensor_id + ';'
        curs.execute(query)
        conn.commit()
        conn.close()
        result = 'true'

    except lite.Error as e:
        logging.error("Error %s:" % e.args[0])
        result = 'false'

    data = json.dumps(result)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def set_max(request, name):

    tmp = name.split('_')
    sensor_id = tmp[0]
    val = tmp[1]

    conn = None
    try:
        conn = lite.connect(status_db)
        curs = conn.cursor()
        query = 'update status set thr_max=' + val + ' where id=' + sensor_id + ';'
        curs.execute(query)
        conn.commit()
        conn.close()
        result = 'true'

    except lite.Error as e:
        logging.error("Error %s:" % e.args[0])
        result = 'false'

    data = json.dumps(result)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


def get_delta_sec():
    conn = None
    try:
        conn = lite.connect(status_db)
        curs = conn.cursor()
        curs.execute("select rate from status where serial_num='location';")
        conn.commit()
        rows = curs.fetchone()
        conn.close()
        if rows:
            return rows[0]
        else:
            return 0

    except lite.Error as e:
        logging.error("Error %s:" % e.args[0])
        return 0


def getdatetime(request):

    delta = get_delta_sec()

    cur_dt = datetime.datetime.now() - datetime.timedelta(0, delta)  # days, seconds, then other fields.

    str_time = cur_dt.strftime("%m/%d/%Y %H:%M")
    data = json.dumps(str_time)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


def get_location():
    """
    get location from the status db
    the location column is : serial_num('location'), alive('off), dev_name('location'), dev_info
    location information is stored at dev_info field
    """
    conn = None
    try:
        conn = lite.connect(status_db)
        curs = conn.cursor()
        curs.execute("select dev_info from status where serial_num='location';")
        conn.commit()
        rows = curs.fetchone()
        conn.close()

        if rows:
            if rows[0] != '':
                return rows[0]

        return 'Not set'

    except lite.Error as e:
        print("Error %s:" % e.args[0])
        logging.error("Error %s:" % e.args[0])
        return 'false'


@login_required(login_url='/login/')
def platform(request, name):
    """
    Return the hostname
    """
    getplatform = get_platform()

    hostname = getplatform['hostname']
    osname = getplatform['osname']
    kernel = getplatform['kernel']
    localip = getplatform['localip']
    data = {}

    if name == 'hostname':
        try:
            data = hostname
        except Exception:
            data = None

    if name == 'osname':
        try:
            data = osname
        except Exception:
            data = None

    if name == 'kernel':
        try:
            data = kernel
        except Exception:
            data = None
    if name == 'location':
        data = get_location()

    if name == 'localip':
        data = localip

    data = json.dumps(data)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


def set_alarm_int(request, name):
    if set_config_param('ALARM_INTERVAL', name):
        data = 'true'
    else:
        data = 'false'
    data = json.dumps(data)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


def get_alarm_int(request):
    tt = get_param_from_xml('ALARM_INTERVAL')
    data = json.dumps(tt)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


def set_alarm_email(request, name):
    tmp = name.replace('-', '@')
    if set_config_param('ALARM_EMAIL', tmp):
        data = 'true'
    else:
        data = 'false'

    data = json.dumps(data)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


def get_alarm_email(request):
    tt = get_param_from_xml('ALARM_EMAIL')
    data = json.dumps(tt)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response

@login_required(login_url='/login/')
def getlastval(request, name):
    """
    Return the hostname
    """
    last_val = get_lastval(name)

    data = json.dumps(last_val)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def setvalue(request, name):
    """
    Return the hostname
    """
    ret_val = set_value(name)

    data = json.dumps(ret_val)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response

@login_required(login_url='/login/')
def getmtctemp(request, name):
    """
    Return the hostname
    """
    ret_val = get_mtc_temp(name)

    data = json.dumps(ret_val)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def getcpus(request, name):
    """
    Return the CPU number and type/model
    """
    cpus = get_cpus()
    cputype = cpus['type']
    cpucount = cpus['cpus']
    data = {}

    if name == 'type':
        try:
            data = cputype
        except Exception:
            data = None

    if name == 'count':
        try:
            data = cpucount
        except Exception:
            data = None

    data = json.dumps(data)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


def save_report_file():
    conn = None
    try:
        conn = lite.connect(status_db)
        curs = conn.cursor()
        curs.execute("SELECT dev_name FROM status where serial_num!='location'")
        rows = curs.fetchall()
        conn.close()

        re_list = []
        if rows:
            for row in rows:
                if row[0][0] != 'f':    # some field has "failed to write" value
                    re_list.append(row[0])

        file_name = BASE_PATH + 'sensor_data.xlsx'
        workbook = xlsxwriter.Workbook(file_name)

        for dev in re_list:
            worksheet = workbook.add_worksheet(dev)

            db_name = BASE_PATH + 'data/' + dev + '.sqlite'
            conn = lite.connect(db_name)
            curs = conn.cursor()
            sql = "select * from '" + dev + "';"
            curs.execute(sql)
            rows = curs.fetchall()

            result_list = []
            # change date/time format to American...
            for d in rows:
                re = list(d)
                time_tmp = datetime.datetime.strptime(re[1], "%Y-%m-%d %H:%M:%S")
                str_time = time_tmp.strftime("%m/%d/%Y %H:%M:%S")
                re[1] = str_time
                result_list.append(re)

            worksheet.write(0, 0, "No.")
            worksheet.write(0, 1, "Date/Time")

            if dev[0] == 'E':  # when EC
                worksheet.write(0, 2, "uS")
                worksheet.write(0, 3, "TDS")
                worksheet.write(0, 4, "PPT")
                worksheet.write(0, 5, "SG")
                worksheet.write(0, 6, "Temperature")
                row = 1
                for item in result_list:
                    for i in range(len(item)):
                        worksheet.write(row, i, item[i])
                    row += 1
            else:
                if dev[0] == 'R':  # when RTD
                    worksheet.write(0, 2, "Temperature")
                else:
                    if dev[0] == 'p':  # when pH
                        worksheet.write(0, 2, "pH")
                    elif dev[0] == 'D':  # when DO
                        worksheet.write(0, 2, "D.O.")
                    elif dev[0] == 'O':  # when ORP
                        worksheet.write(0, 2, "ORP")
                    worksheet.write(0, 3, "Temperature")

                row = 1
                for item in result_list:
                    # pop val2, val3, val4
                    item.pop(3)
                    item.pop(3)
                    item.pop(3)
                    if dev[0] == 'R':  # when RTD, pop temperature column again
                        item.pop(3)

                    for i in range(len(item)):
                        worksheet.write(row, i, item[i])
                    row += 1
            conn.close()

        workbook.close()

        return file_name

    except lite.Error as e:
        logging.error("Error %s:" % e.args[0])
        return False
    except Exception as e:
        logging.error("Error %s:" % e.args[0])
        return False


@login_required(login_url='/login/')
def export(request):

    save_report_file()

    f = open(BASE_PATH + 'sensor_data.xlsx', 'r')
    myfile = File(f)

    now = time.localtime()

    loc = get_location()
    if loc[:3] == "Not":   # when location is not set, its value is not base64
        loc = "Not Set"
    else:   # decode base64
        loc = base64.b64decode(loc.replace('_', '='))   #
        loc = loc.replace(',', '_')     # replace comma to avoid file name error
        loc = loc.replace(' ', '_')     # replace space to avoid file name error

    timestamp = "%04d-%02d-%02d" % (now.tm_year, now.tm_mon, now.tm_mday)
    response = HttpResponse(myfile, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=' + loc + '(' + timestamp + ').xlsx'
    return response


@login_required(login_url='/login/')
def uptime(request):
    """
    Return uptime
    """
    try:
        up_time = get_uptime()
    except Exception:
        up_time = None

    data = json.dumps(up_time)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def getdisk(request):
    """
    Return the disk usage
    """
    try:
        diskusage = get_disk()
    except Exception:
        diskusage = None

    data = json.dumps(diskusage)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def getips(request):
    """
    Return the IPs and interfaces
    """
    try:
        get_ips = get_ipaddress()
    except Exception:
        get_ips = None

    data = json.dumps(get_ips)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def getusers(request):
    """
    Return online users
    """
    try:
        online_users = get_users()
    except Exception:
        online_users = None

    data = json.dumps(online_users)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def getproc(request):
    """
    Return the running processes
    """
    try:
        processes = get_cpu_usage()
        processes = processes['all']
    except Exception:
        processes = None

    data = json.dumps(processes)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def cpuusage(request):
    """
    Return CPU Usage in %
    """
    try:
        cpu_usage = get_cpu_usage()

    except Exception:
        cpu_usage = 0

    cpu = [
        {
            "value": cpu_usage['free'],
            "color": "#0AD11B"
        },
        {
            "value": cpu_usage['used'],
            "color": "#F7464A"
        }
    ]

    data = json.dumps(cpu)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def memusage(request):
    """
    Return Memory Usage in % and numeric
    """
    datasets_free = []
    datasets_used = []
    datasets_buffers = []
    datasets_cached = []

    try:
        mem_usage = get_mem()
    except Exception:
        mem_usage = 0

    try:
        cookies = request.COOKIES['memory_usage']
    except Exception:
        cookies = None

    if not cookies:
        datasets_free.append(0)
        datasets_used.append(0)
        datasets_buffers.append(0)
        datasets_cached.append(0)
    else:
        datasets = json.loads(cookies)
        datasets_free = datasets[0]
        datasets_used = datasets[1]
        datasets_buffers = datasets[2]
        datasets_cached = datasets[3]

    if len(datasets_free) > 10:
        while datasets_free:
            del datasets_free[0]
            if len(datasets_free) == 10:
                break
    if len(datasets_used) > 10:
        while datasets_used:
            del datasets_used[0]
            if len(datasets_used) == 10:
                break
    if len(datasets_buffers) > 10:
        while datasets_buffers:
            del datasets_buffers[0]
            if len(datasets_buffers) == 10:
                break
    if len(datasets_cached) > 10:
        while datasets_cached:
            del datasets_cached[0]
            if len(datasets_cached) == 10:
                break
    if len(datasets_free) <= 9:
        datasets_free.append(int(mem_usage['free']))
    if len(datasets_free) == 10:
        datasets_free.append(int(mem_usage['free']))
        del datasets_free[0]
    if len(datasets_used) <= 9:
        datasets_used.append(int(mem_usage['usage']))
    if len(datasets_used) == 10:
        datasets_used.append(int(mem_usage['usage']))
        del datasets_used[0]
    if len(datasets_buffers) <= 9:
        datasets_buffers.append(int(mem_usage['buffers']))
    if len(datasets_buffers) == 10:
        datasets_buffers.append(int(mem_usage['buffers']))
        del datasets_buffers[0]
    if len(datasets_cached) <= 9:
        datasets_cached.append(int(mem_usage['cached']))
    if len(datasets_cached) == 10:
        datasets_cached.append(int(mem_usage['cached']))
        del datasets_cached[0]

    # Some fix division by 0 Chart.js
    if len(datasets_free) == 10:
        if sum(datasets_free) == 0:
            datasets_free[9] += 0.1
        if sum(datasets_free) / 10 == datasets_free[0]:
            datasets_free[9] += 0.1

    memory = {
        'labels': [""] * 10,
        'datasets': [
            {
                "fillColor": "rgba(247,70,74,0.5)",
                "strokeColor": "rgba(247,70,74,1)",
                "pointColor": "rgba(247,70,74,1)",
                "pointStrokeColor": "#fff",
                "data": datasets_used
            },
            {
                "fillColor": "rgba(43,214,66,0.5)",
                "strokeColor": "rgba(43,214,66,1)",
                "pointColor": "rgba(43,214,66,1)",
                "pointStrokeColor": "#fff",
                "data": datasets_free
            },
            {
                "fillColor": "rgba(0,154,205,0.5)",
                "strokeColor": "rgba(0,154,205,1)",
                "pointColor": "rgba(0,154,205,1)",
                "pointStrokeColor": "#fff",
                "data": datasets_buffers
            },
            {
                "fillColor": "rgba(255,185,15,0.5)",
                "strokeColor": "rgba(255,185,15,1)",
                "pointColor": "rgba(265,185,15,1)",
                "pointStrokeColor": "#fff",
                "data": datasets_cached
            }
        ]
    }

    cookie_memory = [datasets_free, datasets_used, datasets_buffers, datasets_cached]
    data = json.dumps(memory)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.cookies['memory_usage'] = cookie_memory
    response.write(data)
    return response


@login_required(login_url='/login/')
def loadaverage(request):
    """
    Return Load Average numeric
    """
    datasets = []

    try:
        load_average = get_load()
    except Exception:
        load_average = 0

    try:
        cookies = request.COOKIES['load_average']
    except Exception:
        cookies = None

    if not cookies:
        datasets.append(0)
    else:
        datasets = json.loads(cookies)
    if len(datasets) > 10:
        while datasets:
            del datasets[0]
            if len(datasets) == 10:
                break
    if len(datasets) <= 9:
        datasets.append(float(load_average))
    if len(datasets) == 10:
        datasets.append(float(load_average))
        del datasets[0]

    # Some fix division by 0 Chart.js
    if len(datasets) == 10:
        if sum(datasets) == 0:
            datasets[9] += 0.1
        if sum(datasets) / 10 == datasets[0]:
            datasets[9] += 0.1

    load = {
        'labels': [""] * 10,
        'datasets': [
            {
                "fillColor": "rgba(151,187,205,0.5)",
                "strokeColor": "rgba(151,187,205,1)",
                "pointColor": "rgba(151,187,205,1)",
                "pointStrokeColor": "#fff",
                "data": datasets
            }
        ]
    }

    data = json.dumps(load)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.cookies['load_average'] = datasets
    response.write(data)
    return response


@login_required(login_url='/login/')
def gettraffic(request, name):

    try:
        traffic = get_traffic(name)
    except Exception:
        traffic = 0

    data = json.dumps(traffic)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(data)
    return response


@login_required(login_url='/login/')
def getdiskio(request):
    """
    Return the reads and writes for the drive
    """
    datasets_in = []
    datasets_in_i = []
    datasets_out = []
    datasets_out_o = []

    try:
        diskrw = get_disk_rw()
        diskrw = diskrw[0]
    except Exception:
        diskrw = 0

    try:
        cookies = request.COOKIES['diskrw']
    except Exception:
        cookies = None

    if not cookies:
        datasets_in.append(0)
        datasets_in_i.append(0)
        datasets_out.append(0)
        datasets_out_o.append(0)
    else:
        datasets = json.loads(cookies)
        datasets_in = datasets[0]
        datasets_out = datasets[1]
        datasets_in_i = datasets[2]
        datasets_out_o = datasets[3]

    if len(datasets_in) > 10:
        while datasets_in:
            del datasets_in[0]
            if len(datasets_in) == 10:
                break
    if len(datasets_in_i) > 2:
        while datasets_in_i:
            del datasets_in_i[0]
            if len(datasets_in_i) == 2:
                break
    if len(datasets_out) > 10:
        while datasets_out:
            del datasets_out[0]
            if len(datasets_out) == 10:
                break
    if len(datasets_out_o) > 2:
        while datasets_out_o:
            del datasets_out_o[0]
            if len(datasets_out_o) == 2:
                break

    if len(datasets_in_i) <= 1:
        datasets_in_i.append(int(diskrw[1]))
    if len(datasets_in_i) == 2:
        datasets_in_i.append(int(diskrw[1]))
        del datasets_in_i[0]
    if len(datasets_out_o) <= 1:
        datasets_out_o.append(int(diskrw[2]))
    if len(datasets_out_o) == 2:
        datasets_out_o.append(int(diskrw[2]))
        del datasets_out_o[0]

    dataset_in = (int((datasets_in_i[1] - datasets_in_i[0]) / (time_refresh_net / 1000)))
    dataset_out = (int((datasets_out_o[1] - datasets_out_o[0]) / (time_refresh_net / 1000)))

    if len(datasets_in) <= 9:
        datasets_in.append(dataset_in)
    if len(datasets_in) == 10:
        datasets_in.append(dataset_in)
        del datasets_in[0]
    if len(datasets_out) <= 9:
        datasets_out.append(dataset_out)
    if len(datasets_out) == 10:
        datasets_out.append(dataset_out)
        del datasets_out[0]

    # Some fix division by 0 Chart.js
    if len(datasets_in) == 10:
        if sum(datasets_in) == 0:
            datasets_in[9] += 0.1
        if sum(datasets_in) / 10 == datasets_in[0]:
            datasets_in[9] += 0.1

    disk_rw = {
        'labels': [""] * 10,
        'datasets': [
            {
                "fillColor": "rgba(245,134,15,0.5)",
                "strokeColor": "rgba(245,134,15,1)",
                "pointColor": "rgba(245,134,15,1)",
                "pointStrokeColor": "#fff",
                "data": datasets_in
            },
            {
                "fillColor": "rgba(15,103,245,0.5)",
                "strokeColor": "rgba(15,103,245,1)",
                "pointColor": "rgba(15,103,245,1)",
                "pointStrokeColor": "#fff",
                "data": datasets_out
            }
        ]
    }

    cookie_diskrw = [datasets_in, datasets_out, datasets_in_i, datasets_out_o]
    data = json.dumps(disk_rw)
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.cookies['diskrw'] = cookie_diskrw
    response.write(data)
    return response


def get_param_from_xml(param):
    """
    Get configuration parameters from the config.xml
    :param param: parameter name
    :return: if not exists, return None
    """
    conf_file_name = "/home/pi/iniac/config.xml"
    root = xml.etree.ElementTree.parse(conf_file_name).getroot()
    tmp = None
    for child_of_root in root:
        if child_of_root.tag == param:
            tmp = child_of_root.text
            break
    return tmp


def set_config_param(tag_name, new_val):
    """
    set new parameter to config.xml file
    :param tag_name:    Name of new tag
    :param new_val:     new value
    :return:
    """
    conf_file_name = "/home/pi/iniac/config.xml"
    et = xml.etree.ElementTree.parse(conf_file_name)

    for child_of_root in et.getroot():
        if child_of_root.tag == tag_name:
            child_of_root.text = new_val
            et.write(conf_file_name)
            return True
    return False
