from django.conf.urls import patterns, url
from django.conf import settings

# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'iniac.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       # url(r'^admin/', include(admin.site.urls)),
                       url(r'^$', 'main.views.index', name='index'),
                       url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'},
                           name='login'),
                       url(r'^logout/$', 'django.contrib.auth.views.logout', {'template_name': 'logout.html'},
                           name='logout'),
                       url(r'^main/$', 'main.views.getall', name='main'),
                       url(r'^setup/([\w\-\.]+)/$', 'main.views.setup_sensor', name='setup'),
                       url(r'^info/uptime/$', 'usage.views.uptime', name='uptime'),
                       url(r'^info/memory/$', 'usage.views.memusage', name='memusage'),
                       url(r'^info/cpuusage/$', 'usage.views.cpuusage', name='cpuusage'),
                       url(r'^info/getdisk/$', 'usage.views.getdisk', name='getdisk'),
                       url(r'^info/getusers/$', 'usage.views.getusers', name='getusers'),
                       url(r'^info/getips/$', 'usage.views.getips', name='getips'),
                       url(r'^info/clear_all/$', 'usage.views.clearall', name='clearall'),
                       url(r'^info/export/$', 'usage.views.export', name='export'),
                       url(r'^info/gettraffic/([\w\-\.]+)/$', 'usage.views.gettraffic', name='gettraffic'),
                       url(r'^info/setlocation/([\w\-\.]+)/$', 'usage.views.setlocation', name='setlocation'),
                       url(r'^info/proc/$', 'usage.views.getproc', name='getproc'),
                       url(r'^info/getdiskio/$', 'usage.views.getdiskio', name='getdiskio'),
                       url(r'^info/loadaverage/$', 'usage.views.loadaverage', name='loadaverage'),
                       url(r'^info/platform/([\w\-\.]+)/$', 'usage.views.platform', name='platform'),
                       url(r'^info/setvalue/([\w\-\.]+)/$', 'usage.views.setvalue', name='setvalue'),
                       url(r'^info/setrate/([\w\-\.]+)/$', 'usage.views.setrate', name='setrate'),
                       url(r'^info/getcpus/([\w\-\.]+)/$', 'usage.views.getcpus', name='getcpus'),
                       url(r'^info/getlastval/([\w\-\.]+)/$', 'usage.views.getlastval', name='getlastval'),
                       url(r'^info/getmtctemp/([\w\-\.]+)/$', 'usage.views.getmtctemp', name='getmtctemp'),
                       url(r'^info/set_alarm_int/([\w\-\.]+)/$', 'usage.views.set_alarm_int', name='set_alarm_int'),
                       url(r'^info/set_alarm_email/([\w\-\.]+)/$', 'usage.views.set_alarm_email', name='set_alarm_email'),
                       url(r'^info/set_min/([\w\-\.]+)/$', 'usage.views.set_min', name='set_min'),
                       url(r'^info/set_max/([\w\-\.]+)/$', 'usage.views.set_max', name='set_max'),
                       url(r'^info/get_alarm_int/$', 'usage.views.get_alarm_int', name='get_alarm_int'),
                       url(r'^info/get_alarm_email/$', 'usage.views.get_alarm_email', name='get_alarm_email'),

                       url(r'^info/getdatetime/$', 'usage.views.getdatetime', name='getdatetime'),
                       url(r'^info/getnetstat/$', 'usage.views.getnetstat', name='getnetstat'))


urlpatterns += patterns('',
                        (r'^static/(?P<path>.*)$', 'django.views.static.serve',
                         {'document_root': settings.STATIC_ROOT}))
