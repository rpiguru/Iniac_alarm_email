import base64
import os
import time
import sqlite3 as lite
import urlparse

from sparkpost import SparkPost
import sys
import datetime
from iniac_device import IniacDevice
from pylibftdi import Driver
from pylibftdi import FtdiError
import subprocess as sub
from iniac_base import IniacBase

current_path = os.path.dirname(os.path.abspath(__file__))


class IniacEventHandler(IniacBase):

    def __init__(self):
        IniacBase.__init__(self)

    def add_new_device(self, ser_num):
        """
        Add new device to the status table
        :param ser_num: serial number of new device
        :return:
        """
        def get_latest_dev_num(dname):
            dev_name_list = self.query_parser("sn", 'get_device_name_list', True)
            dev_info_len = len(dname)
            cnt = 0
            for tmp in dev_name_list:
                if tmp[:dev_info_len] == dname:      # if new dev_info is included in component...
                    cnt += 1
            return cnt

        try:
            dev = IniacDevice(ser_num)
        except FtdiError:
            print FtdiError.message
            self.add_log('error', 'Critical Error: Cannot initialize device, SN: %s' % ser_num)
            return False

        print "Initializing..."
        dev.init_device()

        dev_name = dev.get_device_name()

        # try to write 3 times, if fails, return false
        cnt_retry = 0
        while dev_name[:2] in ['er', 'fa']:     # error message is 'error, ...' and 'failed to write'
            dev_name = dev.get_device_name()
            if cnt_retry < 3:
                cnt_retry += 1
            else:
                print "Critical Error: Failed to write something to the sensor. SN : ", ser_num
                self.add_log('error', "Critical Error: Failed to write something to the sensor. SN : %s" % ser_num)
                return False

        dev_info = dev.get_device_info()
        if dev_info == 'OR':
            dev_info = 'ORP'
        elif dev_info[:3] == 'OR(':
            dev_info = dev_info.replace('OR', 'ORP')

        print "Information from device:---------------------------------"
        print "Name: ", dev_name
        print "Info: ", dev_info
        print "Serial Number: ", ser_num
        print "---------------------------------------------------------"
        # if this device is never connected before, its name will not contain dev_info
        if dev_name[:len(dev_info)] != dev_info:   # assign new dev_name to the added device
            count = get_latest_dev_num(dev_info)
            if count == 0:                  # when there is no devices with same info
                dev_name = dev_info
            else:                           # add number at the end of info
                dev_name = dev_info + "(" + str(count) + ")"
            dev.set_device_name(dev_name)   # set name of device as given device name
        print "Assigned dev name is ", dev_name
        self.add_log('info', 'SN: %s, Assigned device name is %s' % (ser_num, dev_name))
        del dev

        self.check_device_table(dev_name)    # create new database with given device_name

        dev_id = self.query_parser(dev_name, 'get_id_by_dev_name', True)
        if dev_id:  # this sensor is already registered before
            self.query_parser((ser_num, dev_name), 'update_device_status', True)
        else:
            # insert new device row to the status database
            cur_time = datetime.datetime.now().replace(year=2015)
            rate = self.query_parser(1, 'get_rate', True)       # the initial sampling rate is 5 sec
            sql = "INSERT INTO status (serial_num, dev_info, dev_name, rate, alive, thr_min, thr_max, last_time) " \
                  "VALUES ('" + ser_num + "','" + dev_info + "','" + dev_name + "'," + str(rate) + ", 'on', 0, 1000, '" \
                  + cur_time.strftime("%m/%d/%Y %H:%M:%S") + "');"
            print "Adding new device:"
            print sql

            try:
                self.curs_status.execute(sql)
                self.conn_status.commit()
                return dev_name
            except lite.Error as e:
                print("Error %s:" % e.args[0])
                self.add_log('error', "Error %s:" % e.args[0] + " : " + ser_num)
                return False

    def query_parser(self, _id, mode, val):
        """
        Parse requests on status table
        :param _id:
        :param mode:
        :param val:
        :return:
        """
        # print "Query parser, id : ", _id, "mode : ", mode, "val: ", val

        re_list = []
        try:
            if mode == 'get_device_name_list':
                self.curs_status.execute("SELECT dev_name FROM status")
                rows = self.curs_status.fetchall()
                if rows:
                    for i in range(len(rows)):
                        tmp = str(rows[i])
                        re_list.append(tmp.split("'")[1])
                return re_list
            elif mode == 'get_sn_list':
                self.curs_status.execute("SELECT serial_num FROM status")
                rows = self.curs_status.fetchall()
                if rows:
                    for i in range(len(rows)):
                        tmp = str(rows[i])
                        re_list.append(tmp.split("'")[1])
                return re_list
            elif mode == 'get_active_id_list':
                self.curs_status.execute("SELECT id FROM status where alive='on'")
                rows = self.curs_status.fetchall()
                if rows:
                    for r in rows:
                        re_list.append(r[0])
                return re_list

            elif mode == 'get_location':
                self.curs_status.execute("select dev_info from status where serial_num='location';")
                rows = self.curs_status.fetchone()
                if rows:
                    if rows[0] == '':
                        return "Not Set"
                    else:
                        loc = base64.b64decode(rows[0].replace('_', '='))
                    return loc
                else:
                    return "Not set"

            elif mode == 'get_dev_name_by_id':
                self.curs_status.execute("SELECT dev_name FROM status where id=" + str(_id) + ";")
                rows = self.curs_status.fetchone()
                if rows:
                    return rows[0]
                else:
                    return ''

            elif mode == 'get_cal_id_list':
                self.curs_status.execute("SELECT id FROM status where alive='cal'")
                rows = self.curs_status.fetchall()
                if rows:
                    for r in rows:
                        re_list.append(r[0])
                return re_list

            elif mode == 'get_sensor_by_id':
                self.curs_status.execute("SELECT * FROM status where id=" + str(_id) + ";")
                rows = self.curs_status.fetchone()
                if rows:
                    return rows
                else:
                    return ''

            elif mode == 'get_rate':
                self.curs_status.execute("SELECT rate FROM status;")
                rows = self.curs_status.fetchone()
                if rows:
                    return rows[0]
                else:
                    return 10

            elif mode == 'get_sn_by_id':
                self.curs_status.execute("SELECT serial_num FROM status where id=" + str(_id) + ";")
                rows = self.curs_status.fetchone()
                if rows:
                    return rows[0]
                else:
                    return ''

            elif mode == 'get_id_by_dev_name':
                self.curs_status.execute("SELECT id FROM status where dev_name='" + _id + "';")
                rows = self.curs_status.fetchone()
                if rows:
                    return rows[0]
                else:
                    return None

            elif mode == 'clear_device_status':
                self.curs_status.execute("DELETE FROM status;")
                self.conn_status.commit()
                print('clearing device status...')
                return True

            elif mode == 'update_last_time':
                query = "UPDATE status SET last_time = '" + val + "' WHERE id = '" + str(_id) + "';"
                self.curs_status.execute(query)
                self.conn_status.commit()

            elif mode == 'update_device_status':
                if val:
                    # in this case, _id is tuple (sn, dev_name)
                    sn = _id[0]
                    dev_name = _id[1]
                    # must update sn as well
                    query = "UPDATE status SET alive = 'on', serial_num='" + sn + "' WHERE dev_name = '" + dev_name + "';"
                else:
                    query = "UPDATE status SET alive = 'off' WHERE dev_name = '" + str(_id) + "';"
                self.curs_status.execute(query)
                self.conn_status.commit()
                return True
            elif mode == 'update_device_status_by_sn':
                if val:
                    query = "UPDATE status SET alive = 'on' WHERE serial_num = '" + _id + "';"
                else:
                    query = "UPDATE status SET alive = 'off' WHERE serial_num = '" + _id + "';"
                self.curs_status.execute(query)
                self.conn_status.commit()
                return True
            elif mode == 'create_location':
                query = 'insert into status (serial_num, alive, dev_name, dev_info, rate)' \
                        ' values ("location", "off", "location", "", ' + str(time.altzone) + ')'
                self.curs_status.execute(query)
                self.conn_status.commit()
                return True

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            self.add_log('error', "Error %s:" % e.args[0] + " : " + _id)

    def get_connected_dev_list(self):
        """
        return a list of lines, each a colon-separated
        vendor:product:serial summary of detected devices
        """
        sn_list = []

        for device in Driver().list_devices():
            # list_devices returns bytes rather than strings
            dev_info = map(lambda x: x.decode('latin1'), device)
            # device must always be this triple
            vendor, product, serial = dev_info
            sn_list.append(serial)

        name_list = []
        for sn in sn_list:
            try:
                dev = IniacDevice(sn)
                dev.init_device()
                name_list.append(dev.get_device_name())
            except FtdiError:
                print FtdiError.message
                continue
        return [sn_list, name_list]

    def get_rate(self):
        """
        Get the reading rate of sensors - only get 1st sensor's value (assuming all ratings are same)
        :return:
        """
        try:
            query = "select rate from status where serial_num!= 'location' ORDER BY id DESC LIMIT 1;"
            self.curs_status.execute(query)
            rate = self.curs_status.fetchone()
            return rate[0]

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            self.add_log('error', "Getting rate from DB failed...")

    def check_device_table(self, device_name):
        """
        check whether table of a sensor exists
        if not, create to database directory
        :param device_name:
        :return:
        """
        conn = None
        try:
            db_name = current_path + '/data/' + device_name + '.sqlite'

            conn = lite.connect(db_name)
            os.system('chmod 777 "' + db_name + '"')

            curs = conn.cursor()
            sql = 'create table if not exists "' + device_name + '" ' \
                  '(' \
                  'id INTEGER PRIMARY KEY AUTOINCREMENT, ' \
                  'timestamp DATETIME, ' \
                  'val1 NUMERIC, ' \
                  'val2 NUMERIC, ' \
                  'val3 NUMERIC, ' \
                  'val4 NUMERIC, ' \
                  'temp TEXT ' \
                  ');'
            curs.execute(sql)
            conn.commit()
    #        print('creating database of ' + db_name)
    #        self.add_log('info', device_name + " : DB has been created.")
            conn.close()
        except lite.Error as e:
            print("Error %s:" % e.args[0])
            self.add_log('error', device_name + " : Creating DB failed...")
        finally:
            if conn:
                conn.close()

    def get_temp_from_rtd(self, id_list):
        dev_list = []
        _id_list = id_list
        for ii in id_list:
            dev_list.append(self.query_parser(ii, 'get_dev_name_by_id', True))

        rtd_active = False
        # move RTD to the first of the list
        for i in range(len(dev_list)):
            if dev_list[i][0] == 'R':  # the 4th value is dev_name
                dev_list.insert(0, dev_list.pop(i))
                _id_list.insert(0, _id_list.pop(i))
                rtd_active = True

        if rtd_active:
            id_rtd = _id_list[0]
            try:
                sn_rtd = self.query_parser(id_rtd, 'get_sn_by_id', True)
                dev = IniacDevice(sn_rtd)

                last_tmp = dev.get_val()
                if not is_number(last_tmp):  # when error occurred, try again
                    last_tmp = dev.get_val()
                del dev
                return last_tmp
            except FtdiError:
                print FtdiError.message
                return None
        else:
            return None

    def read_data(self, _id, rtd_temp, dev_name):

        sn = self.query_parser(_id, 'get_sn_by_id', True)
        try:
            dev = IniacDevice(sn)
        except FtdiError:
            print FtdiError.message
            return False

        self.check_device_table(dev_name)
        db_name = current_path + "/data/" + dev_name + ".sqlite"

        conn = None
        try:
            conn = lite.connect(db_name)
            curs = conn.cursor()

            if dev_name[0] != 'R':  # when not RTD
                if rtd_temp:
                    dev.set_temp_compensation(rtd_temp)
                    time.sleep(0.1)
                    temp = rtd_temp
                else:   # when RTD is not active now
                    temp = ''
            else:
                temp = 'N/A'

            val = dev.get_val()

            del dev

            if not is_number(val) and val.find(',') == -1:  # when error occured, do not upload data to db
                conn.close()
                return False
            elif val == '0':  # when failed to read
                conn.close()
                return False
            else:
                val_list = val.split(',')
                if len(val_list) < 4:
                    for i in range(4 - len(val_list)):
                        val_list.append('0')

                query = "INSERT INTO '" + dev_name + "' (timestamp, val1, val2, val3, val4, temp)" \
                                                     " values(datetime('now','localtime'), " \
                        + val_list[0] + ", " + "" \
                        + val_list[1] + ", " + "" \
                        + val_list[2] + ", " + "" \
                        + val_list[3] + ", " + "'" \
                        + temp + "');"
                print(query)
                #            self.add_log('info', dev_name + " : query : " + query)
                curs.execute(query)
                conn.commit()
                #            self.add_log('info', dev_name + " : Succeed to update device status in DB.")
                conn.close()

                self.check_min_max(_id, dev_name, float(val_list[0]))

                return True
        except lite.Error as e:
            print("Error %s:" % e.args[0])
            self.add_log('error', "Error %s:" % e.args[0] + " : " + dev_name)
            del dev
            return False

    def check_min_max(self, _id, dev_name, val):
        """
        Check the last value whether greater than max value or smaller than min value and send email.
        :param _id: id of sensor
        :param val: read val
        """
        inst_sensor = self.query_parser(_id, 'get_sensor_by_id', True)
        print "Sensor Data: ", inst_sensor
        thr_min = inst_sensor[6]
        thr_max = inst_sensor[7]
        last_time = datetime.datetime.strptime(inst_sensor[8], "%m/%d/%Y %H:%M:%S")
        time_delta = datetime.datetime.now() - last_time

        if val > thr_max or val < thr_min:

            print val, "Min: ", thr_min, "Max: ", thr_max, "Last: ", last_time, "D: ", time_delta.total_seconds()

            if time_delta.total_seconds() < int(self.get_param_from_xml('ALARM_INTERVAL')) * 60:
                print "email is already sent."
                return True

            print "Sending error mail..."
            loc = self.query_parser(_id, 'get_location', True)
            msg = "Sensor: " + dev_name + "<br>Location: " + loc + "<br>Current value: " + str(val) + \
                  "<br>Max Threshold: " + str(thr_max) + "<br>Min Threshold: " + str(thr_min)
            print "Sending email: ", msg
            if self.send_mail(msg):
                self.query_parser(_id, 'update_last_time', datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S"))
            else:
                print "Failed to send mail."

    def get_mtc_temp(self, _id):
        try:
            query = "SELECT mtc_temp FROM tmp WHERE dev_id=" + str(_id) + ";"
            self.curs_tmp.execute(query)
            rows = self.curs_tmp.fetchone()
            if rows:
                return rows[0].split('_')
            else:
                return None

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            self.add_log('error', "Getting mtc_temp from tmp table failed...")
            return None

    def init_all(self):
        print "Starting initialization..."

        # sync all lists
        # get sn list from status table
        name_list_db = self.query_parser('', 'get_device_name_list', True)

        connected_dev_list = self.get_connected_dev_list()  # (sn_list, name_list)

        if not name_list_db:  # if there is no registered device
            if connected_dev_list[1]:
                for i in range(len(connected_dev_list[0])):
                    # create table, add sn to the list and set its status to ON
                    self.add_new_device(connected_dev_list[0][i])
                # set location values
                self.query_parser("", 'create_location', True)

        else:
            if not connected_dev_list:  # if there is no sensors connected, mark all sensors as 'False'
                for dev_db in name_list_db:
                    if dev_db != 'location':
                        self.query_parser(dev_db, 'update_device_status', False)
                        print "updated device status as false : ", dev_db
            else:
                for dev_db in name_list_db:
                    if dev_db in connected_dev_list[1]:
                        index = connected_dev_list[1].index(dev_db)
                        sn = connected_dev_list[0][index]
                        self.query_parser((sn, dev_db), 'update_device_status', True)
                    else:
                        if dev_db != 'location':
                            self.query_parser(dev_db, 'update_device_status', False)
                            print "updated device status as false : ", dev_db
        return True

    def start(self):
        while True:
            start_time = time.time()

            active_id_list_db = self.query_parser('', 'get_active_id_list', True)
            cal_id_list = self.query_parser('', 'get_cal_id_list', True)

            if not active_id_list_db:   # if there is no active devices
                time.sleep(3)
            else:
                # update values in active sensor list
                last_temp = self.get_temp_from_rtd(active_id_list_db)
                print "LAST TEMP : ", last_temp
                print "Cal List : ", cal_id_list

                for _id in active_id_list_db:
                    if not _id in cal_id_list:       # if this device is not in calibrating status now.
                        dev_name = self.query_parser(_id, 'get_dev_name_by_id', True)
                        temp_type = self.get_mtc_temp(_id)

                        if temp_type:
                            if temp_type[0] == 'mtc':
                                self.read_data(_id, temp_type[1], dev_name)
                            else:
                                self.read_data(_id, last_temp, dev_name)
                        else:
                            # if row of this sensor in tmp table is not created yet, ignore manual temp and set as auto
                            self.read_data(_id, last_temp, dev_name)

                time_interval = self.get_rate()
                print "time rate is ", time_interval

                print "Elapsed time : ", time.time() - start_time

                while True:
                    if time.time() - start_time > time_interval:
                        break
                    else:
                        time.sleep(0.1)

    def send_mail(self, msg):
        """
        Send video file to the e-mail
        """
        # get location.
        email_address = self.get_param_from_xml("ALARM_EMAIL")

        sp = SparkPost('830385599a4c07578c1d42981050d97bd81d9612')

        content = '<p>Hi!</p><br><p>The value exceeds the threshold!!!</p><br><p>' + msg + \
                  '</p><br><p>Thanks.</p>'

        response = sp.transmissions.send(
            recipients=[email_address],
            html=content,
            from_email='epierre@awakeningwater.com',
            subject='The value exceeds the threshold!!!',
            track_opens=True,

        )

        print(response)
        if response.get('total_accepted_recipients') == 1:
            return True
        else:
            return False


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


if __name__ == '__main__':

    ctrl = IniacEventHandler()
    ctrl.add_log('info', 'Iniac Software is started...')

    # run 3rd party scripts
    # sub.Popen("python iniac_watchdog.py", shell=True)   # action script which runs when user opens setup page
    # sub.Popen('python iniac_parse_cmd.py', shell=True)  # action script which runs when user send any setting request
    # sub.Popen('python iniac_monitor.py', shell=True)    # action script for monitoring plug in/unplug

    ctrl.init_all()

    ctrl.start()











