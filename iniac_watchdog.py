import sys, os, time, stat, datetime
import logging
import sqlite3 as lite
from iniac_device import IniacDevice
from pylibftdi import FtdiError
from iniac_base import IniacBase


class IniacWatchDog(IniacBase):

    def __init__(self):
        IniacBase.__init__(self)

    def query_parser(self, dev_id, mode):
        try:
            if mode == 'get_all_rows':
                self.curs_tmp.execute("SELECT * FROM tmp")
                rows = self.curs_tmp.fetchall()
                re_list = []
                if rows:
                    for row in rows:
                        re_list.append(list(row))
                return re_list

            if mode == 'disable_state':
                query = "UPDATE tmp SET status='off' WHERE dev_id='" + dev_id + "';"
                self.curs_tmp.execute(query)
                print query
                self.conn_tmp.commit()
                return True

            elif mode == 'update_last_val':
                try:
                    query = "SELECT serial_num from status where id='" + dev_id + "';"
                    self.curs_status.execute(query)
                    rows = self.curs_status.fetchone()
                    ser_num = rows[0]
                    dev = IniacDevice(ser_num)
                    dev_val = dev.get_val()

                    # when error occurred, do not upload data to db
                    if not isNumber(dev_val) and dev_val.find(',') == -1:
                        return False

                    query = "UPDATE tmp SET last_val = '" + dev_val + "' WHERE dev_id = '" + dev_id + "';"
                    print query

                    self.curs_tmp.execute(query)
                    self.conn_tmp.commit()
    #                logging.info(sn + " : Succeed to update last value in DB.")
                    return True
                except FtdiError:
                    print FtdiError.message
                    return False

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            logging.error("Error %s:" % e.args[0] + " : " + dev_id)

    def set_con_mode(self, dev_id, bflag):
        """
        update status of a sensor
        :param dev_id:
        :param bflag:
        :return:
        """
        try:
            if bflag:
                query = "UPDATE status SET alive = 'cal' WHERE id = " + dev_id + ";"
            else:
                query = "UPDATE status SET alive = 'on' WHERE id = " + dev_id + ";"

            print query

            self.curs_status.execute(query)
            self.conn_status.commit()
            # logging.info(sn + " : Succeed to update alive to status DB.")
            return True

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            logging.error("Error %s:" % e.args[0] + " : " + dev_id)


def isNumber(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def get_elaped_time(val):
    """
    get time delta from given timestamp
    """
    start_time = datetime.datetime.strptime(val, "%Y-%m-%d %H:%M:%S")
    off_time = datetime.datetime.utcnow() - start_time
    ret = off_time.seconds
    print "time offset is ", off_time, "start time : ", start_time, "now : ", datetime.datetime.utcnow()
    return ret


if __name__ == '__main__':

    ctrl = IniacWatchDog()
    con_mode_list = []

    counter = 0

    while True:

        tmp_list = ctrl.query_parser('', 'get_all_rows')

        if tmp_list:
            for tmp in tmp_list:            # id, sn, status, last_val, last_time, mtc_temp
                d_id = tmp[1]
                status = tmp[2]
                last_val = tmp[3]
                last_time = tmp[4]

                if tmp[2] == 'on':
                    if d_id in con_mode_list:
                        elaped_time = get_elaped_time(last_time)
                        if elaped_time > 6:    # if do not send request for 6 seconds, close continuous reading mode
                            ctrl.set_con_mode(d_id, False)
                            ctrl.query_parser(d_id, 'disable_state')
                            con_mode_list.remove(d_id)
                        else:
                            ctrl.query_parser(d_id, "update_last_val")

                    else:
                        con_mode_list.append(d_id)
                        ctrl.set_con_mode(d_id, True)
                        ctrl.query_parser(d_id, "update_last_val")

        time.sleep(1)





