"""
    AtlasDevice : python wrapper for Atlas Devices.
    03-08-2016

"""
from pylibftdi.device import Device
from pylibftdi.driver import FtdiError
import iniac_cmd_list as cmd_list
import os
import time


class IniacDevice(Device):

    _b_resp_code = False        # We set initial value as False since we set its value false in init_device function

    @property
    def b_resp_code(self):
        return self._b_resp_code

    @b_resp_code.setter
    def b_resp_code(self, value):
        self.set_resp_code(value)
        self._b_resp_code = value

    def read_line(self):
        lsl = len(os.linesep)
        line_buffer = []
        try:
            start_time = time.time()
            while True:
                next_char = self.read(1)
                if next_char == "\r":
                    break
                line_buffer.append(next_char)
                if (len(line_buffer) >= lsl and
                        line_buffer[-lsl:] == list(os.linesep)):
                    break
                if time.time() - start_time > 1.0:  # timeout
                    line_buffer = ''
                    break

            return ''.join(line_buffer)
        except FtdiError:
            return '0'

    def send_cmd(self, cmd):
        buf = cmd + "\r"
        try:
            self.write(buf)
            return 1
        except FtdiError:
            return 0

    def parse_resp(self, cmd):
        if self.send_cmd(cmd) == 0:         # send command
            return False
        else:
            if self.b_resp_code:
                val = self.read_line()
                if val == cmd_list.resp_OK:
                    return True
                else:
                    return False
            else:
                return True

# Initializing Device
    def init_device(self):
        self.mode = 't'
        time.sleep(0.1)
        self.b_resp_code = False
        time.sleep(0.1)
        self.read_line()  # pop all buffered values
        time.sleep(0.1)
        self.set_con_read_mode(False)
        time.sleep(0.1)
        self.read_line()  # pop all buffered values
        time.sleep(0.1)
        self.lock_device()
        time.sleep(0.1)
        self.read_line()  # pop all buffered values

# Lock sensor to prevent to switch to I2C mode accidently
    def lock_device(self):
        cmd = cmd_list.cmd_lock
        self.parse_resp(cmd)

# commands for RESPONSE
    def set_resp_code(self, mode):
        if mode:
            cmd = cmd_list.cmd_enable_resp_code
        else:
            cmd = cmd_list.cmd_disable_resp_code

        if self.send_cmd(cmd) == 0:         # send command
            return False
        else:
            return True

    def get_resp_code(self):
        cmd = cmd_list.cmd_get_resp_code_status

        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            val = self.read_line()
            if val == cmd_list.resp_resp_code_on:
                return 'on'
            elif val == cmd_list.resp_resp_code_off:
                return 'off'
            else:
                return 'error, received val is ' + val

# commands for LED
    def set_led_mode(self, mode):
        if mode:     # when enabling LED
            cmd = cmd_list.cmd_enable_LED
        else:
            cmd = cmd_list.cmd_disable_LED
        if self.send_cmd(cmd) == 0:         # send command
            return False
        else:
            return True

    def get_led_status(self):
        cmd = cmd_list.cmd_get_LED_status
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            recv_val = self.read_line()
            if recv_val == cmd_list.resp_LED_on:
                return 'on'
            elif recv_val == cmd_list.resp_LED_off:
                return 'off'
            else:
                return 'error, received val is ' + recv_val

# commands for Continuous reading
    def set_con_read_mode(self, mode):
        if mode:
            cmd = cmd_list.cmd_enable_con_read
            return self.parse_resp(cmd)
        else:
            cmd = cmd_list.cmd_disable_con_read
            if self.send_cmd(cmd) == 0:         # send command
                return False
            else:
                return True

    def get_con_read_mode(self):
        cmd = cmd_list.cmd_get_con_read_status
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            recv_val = self.read_line()
            if recv_val == cmd_list.resp_con_read_on:
                return 'on'
            elif recv_val == cmd_list.resp_con_read_off:
                return 'off'
            else:
                return 'error, received val is ' + recv_val


# Single Read
    def get_val(self):
        cmd = cmd_list.cmd_single_read
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            recv_val = self.read_line()
            if "." not in recv_val:  # convert 5 to 5.00
                recv_val += ".00"
            return recv_val

# get device info
    def get_device_info(self):
        cmd = cmd_list.cmd_get_device_info
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            while True:
                recv_val = self.read_line()
                recv_list = recv_val.split(',')
                if recv_list[0] == cmd_list.resp_info:    # if the 1st value is "?I"
                    dev_type = recv_list[1]
                    return dev_type


# Status of Device
    def get_status(self):
        cmd = cmd_list.cmd_get_status
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            recv_val = self.read_line()
            recv_list = recv_val.split(',')
            if recv_list[0] == cmd_list.resp_reason:  # if the 1st value is "?STATUS"
                cur_status = recv_list[1]
                voltage = recv_list[2]
                return cur_status + ':' + voltage
            else:
                return 'error, received val is ' + recv_val

# Device Name
    def set_device_name(self, dev_name):
        cmd = cmd_list.cmd_set_device_name + dev_name
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            return True

    def get_device_name(self):
        cmd = cmd_list.cmd_get_device_name
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            recv_val = self.read_line()
            recv_list = recv_val.split(',')
            if recv_list[0] == cmd_list.resp_device_name:  # if the 1st value is "?NAME"
                dev_name = recv_list[1]
                return dev_name
            else:
                return 'error, received val is ' + recv_val

# Factory Reset
    def set_factory_rst(self):
        cmd = cmd_list.cmd_factory_rst
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            recv_val = self.read_line()
            if recv_val == cmd_list.resp_factory_rst:  # if the 1st value is "*RE"
                return 'true'

# Low Power state
    def goto_sleep(self):
        cmd = cmd_list.cmd_low_power
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            recv_val = self.read_line()
            if recv_val == cmd_list.resp_sleep:  # if the 1st value is "*SL"
                return 'true'
            elif recv_val == cmd_list.resp_wake_up:  # if the 1st value is "*WA"
                return 'wake up'

# I2C
    def goto_i2c(self, address):
        cmd = cmd_list.cmd_goto_i2c + address
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            recv_val = self.read_line()
            if recv_val == cmd_list.resp_i2c_ok:  # if the 1st value is "*RS"
                return 'ok'
            elif recv_val == cmd_list.resp_i2c_error:  # if the 1st value is "*ER"
                return 'wake up'

# change baudrates
    def set_baudrate(self, baudrate):
        cmd = cmd_list.cmd_set_baud_rate + str(baudrate)
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            return True

# Temperature Compensation
    def set_temp_compensation(self, temp_val):
        cmd = cmd_list.cmd_set_temperature + str(temp_val)
        return self.parse_resp(cmd)

    def get_temp_compensation(self):
        cmd = cmd_list.cmd_get_temperature
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            recv_val = self.read_line()
            recv_list = recv_val.split(',')
            if recv_list[0] == cmd_list.resp_temperature:
                return recv_list[1]
            else:
                return 'N/A'

# Calibration
    def cal_clear(self):
        cmd = cmd_list.cmd_cal_clear
        return self.parse_resp(cmd)

    def get_cal(self):
        cmd = cmd_list.cmd_get_cal
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            recv_val = self.read_line()
            recv_list = recv_val.split(',')
            if recv_list[0] == cmd_list.resp_cal:
                return recv_list[1]
            else:
                return 'N/A'

#    ------  D.O. Sensor  ------
# Salinity Compensation
    def set_salinity_us(self, sal_val):
        cmd = cmd_list.cmd_set_salinity + str(sal_val)
        return self.parse_resp(cmd)

    def set_salinity_ppt(self, sal_val):
        cmd = cmd_list.cmd_set_salinity + ("%0.2f" % sal_val) + ',ppt'
        return self.parse_resp(cmd)

    def get_salinity(self):
        cmd = cmd_list.cmd_get_salinity
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            recv_val = self.read_line()
            recv_list = recv_val.split(',')
            if recv_list[2] == 'ppt':
                return 'ppt:' + recv_list[1]
            else:
                return 'uS:' + recv_list[1]

# Pressure Compentations
    def set_pressure(self, pre_val):
        cmd = cmd_list.cmd_set_pressure + str(pre_val)
        return self.parse_resp(cmd)

    def get_pressure(self):
        cmd = cmd_list.cmd_get_pressure
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            recv_val = self.read_line()
            recv_list = recv_val.split(',')
            if recv_list[0] == cmd_list.resp_pressure:
                return recv_list[1]
            else:
                return 'error, received val is ' + recv_val

# Saturation Percentage
    def set_saturation(self, mode):
        if mode:
            cmd = cmd_list.cmd_enable_saturation
        else:
            cmd = cmd_list.cmd_disable_saturation
        return self.parse_resp(cmd)

    def get_saturation(self):
        cmd = cmd_list.cmd_get_saturation_status
        if self.send_cmd(cmd) == 0:
            return 'failed to write'
        else:
            recv_val = self.read_line()
            if recv_val == cmd_list.resp_saturation_on:
                return 'on'
            else:
                return 'off'

# Calibration
    def set_atmospheric(self):
        cmd = cmd_list.cmd_cal_atmospheric
        return self.parse_resp(cmd)

    def set_0_dissolved(self):
        cmd = cmd_list.cmd_cal_0_dissolved
        return self.parse_resp(cmd)


#    ------  pH Sensor  ------

    def set_cal_low(self):
        cmd = cmd_list.cmd_cal_low
        return self.parse_resp(cmd)

    def set_cal_mid(self):
        cmd = cmd_list.cmd_cal_mid
        return self.parse_resp(cmd)

    def set_cal_high(self):
        cmd = cmd_list.cmd_cal_high
        return self.parse_resp(cmd)


#    ------  EC Sensor  ------

    def set_k_probe(self, val):
        cmd = cmd_list.cmd_set_K_val + val
        return self.parse_resp(cmd)

    def set_cal_dry(self):
        cmd = cmd_list.cmd_cal_dry
        return self.parse_resp(cmd)

    def set_cal_dual_high(self, val):
        cmd = cmd_list.cmd_cal_dual_high_n + val
        return self.parse_resp(cmd)

    def set_cal_dual_low(self, val):
        cmd = cmd_list.cmd_cal_dual_low_n + val
        return self.parse_resp(cmd)

#    ------  ORP Sensor  ------
    def set_OR_cal(self, val):
        cmd = cmd_list.cmd_set_cal + val
        return self.parse_resp(cmd)
