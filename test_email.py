from sparkpost import SparkPost


def send_mail(msg):
    """
    Send video file to the e-mail
    """
    email_address = "lew.dudek@polandmail.com"

    sp = SparkPost('830385599a4c07578c1d42981050d97bd81d9612')

    content = "<p>Hey!</p><p>This is the email sent from the Iniac System on your Raspberry Pi.</p>" \
              "<p> As you can see, the sender's email is incorrect. Please check the API security Key in Sparkpost.</p>" \
              "<p>Maybe it is changed after you verify your domain name.</p>"

    response = sp.transmissions.send(
        recipients=[email_address],
        html=content,
        from_email='epierre@awakeningwater.com',
        subject='Service Alert - Location is not set.',
        track_opens=True,

    )

    print(response)


send_mail('Hi')
