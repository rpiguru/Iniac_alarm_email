import signal
import stat

import subprocess

from iniac_device import IniacDevice
import time, sys, os
import sqlite3 as lite
from pylibftdi import FtdiError
import base64
import datetime
from iniac_base import IniacBase
from pylibftdi import Driver


class IniacCMDParser(IniacBase):

    def __init__(self):
        IniacBase.__init__(self)

    def get_cmd_from_db(self):
        """
        get a pending request from the cmd_table
        :return:
        """
        try:
            sql = 'select * from table_cmd'
            self.curs_cmd.execute(sql)
            row = self.curs_cmd.fetchone()
            return row
        except lite.Error as e:
            print("Error %s:" % e.args[0])
            return None

    def get_sn_name_by_id(self, _id):
        """
        get device name from give id
        :param _id:
        :return:
        """
        try:
            query = "SELECT serial_num, dev_name FROM status WHERE id=" + _id + ";"
            self.curs_status.execute(query)
            rows = self.curs_status.fetchone()
            if rows:
                return rows
            else:
                return None

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            return None

    def get_temp_by_probe(self):
        """
        get temperature from the first RTD
        :return:
        """
        def get_sn_of_1st_active_rtd():
            """
            get serial number of 1st active RTD from the status table
            """
            try:
                query = "select serial_num, dev_name from status WHERE alive='on'"
                self.curs_status.execute(query)
                rows = self.curs_status.fetchall()
                if rows:
                    for row in rows:
                        if row[1][0] == 'R':    # if TRD
                            return row[0]
                    return rows[0][0]
                else:
                    return None

            except lite.Error as e:
                print("Error %s:" % e.args[0])
                return None

        sn = get_sn_of_1st_active_rtd()

        try:
            dev = IniacDevice(sn)
        except FtdiError:
            print FtdiError.message
            return None
        temp = dev.get_temp_compensation()

        del dev

        if temp == '0' or ('?' in temp):  # error occurred in reading
            return None
        print "RTD value: ", temp
        return temp

    def update_mtc_temp(self, dev_id, t_type, t_val):
        """
        update atc_temp field of tmp table
        this is for displaying MTC TEMP values at Temperature part
        :param dev_id:
        :param t_type:
        :param t_val:
        :return:
        """
        try:
            if t_type == 'atc':
                query = "UPDATE tmp SET mtc_temp='atc_" + t_val + "' WHERE dev_id='" + dev_id + "';"
            else:
                query = "UPDATE tmp SET mtc_temp='mtc_" + t_val + "' WHERE dev_id='" + dev_id + "';"

            print query
            self.curs_tmp.execute(query)
            self.conn_tmp.commit()
            return True

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            return False

    def excute_cmd(self, request):
        """
        parse the request and execute
        :param request:
        :return:
        """
        # when cmd is of changing date and time, type is location-val
        if request[0] == 'l':
            val_list = request.split('-')
            val = val_list[1].replace('_', '=')
            tmp = base64.b64decode(val)
            print 'New time: ', tmp
            if tmp != '':
                try:
                    new_dt = datetime.datetime.strptime(tmp, "%m/%d/%Y %H:%M")
                    set_system_time(new_dt)
                except ValueError:
                    print 'Invalid type'
                    return False

            return True

        # example of request is dev_id-4__cal_high__temptype-mtc__temp-25.6
        # '__' : distinguish params,  '-' : indicates value
        # type of factory reset command is dev_id-all__factoryrst
        if request == 'dev_id-all__factoryrst':
            print "Reseting all devices..."
            return self.reset_all_devices()

        req_list = request.split('__')
        dev_id = req_list[0].split('-')[1]
        sn, dev_name = self.get_sn_name_by_id(dev_id)
        if not dev_name:
            return False

        try:
            dev = IniacDevice(sn)
        except FtdiError:
            print FtdiError.message
            return False

        ret = False
        cmd = req_list[1]

        param2 = None
        if len(req_list) > 2:
            param2 = req_list[2].split('-')[1]

        if cmd == 'cal_clear':
            ret = dev.cal_clear()
            return ret
        if cmd == 'temp':
            temp_type = req_list[3].split('-')[1]
            self.update_mtc_temp(dev_id, temp_type, param2)
            if temp_type == 'mtc':
                ret = dev.set_temp_compensation(param2)
            else:
                ret = dev.set_temp_compensation(self.get_temp_by_probe())
            return ret

        if dev_name[0] == 'p':   # when pH
            if cmd == 'cal_high':
                ret = dev.set_cal_high()
            elif cmd == 'cal_mid':
                ret = dev.set_cal_mid()
            elif cmd == 'cal_low':
                ret = dev.set_cal_low()

        elif dev_name[0] == 'D':  # when DO
            if cmd == 'cal_1':                  # cal atmosphere
                ret = dev.set_atmospheric()
            elif cmd == 'cal_0':
                ret = dev.set_0_dissolved()
            elif cmd == 'press':                # setting water pressure
                ret = dev.set_pressure(param2)
            elif cmd == 'sal':
                ret = dev.set_salinity_us(param2)
            elif cmd == 'sal-ppt':
                print param2
                ret = dev.set_salinity_ppt(float(param2))

        elif dev_name[0] == 'E':  # when EC
            if cmd == 'probe':
                ret = dev.set_k_probe(param2)
            elif cmd == 'cal_low':
                ret = dev.set_cal_dual_low(param2)
            elif cmd == 'cal_high':
                ret = dev.set_cal_dual_high(param2)
            elif cmd == 'cal_dry':
                ret = dev.set_cal_dry()

        elif dev_name[0] == 'O':  # when OR
            if cmd == 'cal':
                ret = dev.set_OR_cal(param2)
            elif cmd == 'cal_clear':
                ret = dev.set_cal_dry()

        elif dev_name[0] == 'R':    # RTD
            if cmd == 'cal':
                ret = dev.set_OR_cal(param2)    # command is same with OR's calibration

        return ret

    def delete_row(self, _id):
        """
        delete row with given id in cmd_tables
        :param _id:
        :return:
        """
        try:
            sql = "DELETE FROM table_cmd WHERE id=%s" % _id
            self.curs_cmd.execute(sql)
            self.conn_cmd.commit()
            return True
        except lite.Error as e:
            print("Error %s:" % e.args[0])
            return False

    def reset_all_devices(self):
        """
        Reset all connected devices by sending "FACTORY RESET" command.
        And remove all data from status table as well.
        :return:
        """
        # kill running process
        kill_proc("iniac_event_handler")
        kill_proc("iniac_watchdog")
        kill_proc("iniac_monitor")

        # get serial number of all connected devices and send factory reset command
        try:
            sn_list = []

            for device in Driver().list_devices():
                # list_devices returns bytes rather than strings
                dev_info = map(lambda x: x.decode('latin1'), device)
                # device must always be this triple
                vendor, product, serial = dev_info
                sn_list.append(serial)

            for sn in sn_list:
                dev = IniacDevice(sn)
                if dev.set_factory_rst():
                    dev.set_device_name("")

            # remove db files
            path = "/home/pi/iniac/data/"
            exts = ('.sqlite',)
            for root, dirs, files in os.walk(path):
                print files
                for currentFile in files:
                    if currentFile == 'status.sqlite':
                        print "Dropping status table..."
                        self.curs_status.execute("DROP TABLE IF EXISTS status")
                        self.conn_status.commit()
                    elif currentFile == 'cmd.sqlite':
                        print "Dropping cmd table..."
                        self.curs_cmd.execute("DROP TABLE IF EXISTS table_cmd")
                        self.conn_cmd.commit()
                    else:
                        if any(currentFile.lower().endswith(ext) for ext in exts):
                            print "Deleting ", currentFile
                            os.remove(os.path.join(root, currentFile))
            return True

        except FtdiError:
            print FtdiError.message
            return False
        except lite.Error as e:
            print("Error %s:" % e.args[0])
            return False
        except IOError as e:
            print("Error %s:" % e.args[0])
            return False


def kill_proc(process_name):
    pid_list = []

    cmd_result = os.popen("ps ax | grep " + process_name + " | grep -v grep")

    for line in cmd_result:
        fields = line.split()
        pid_list.append(fields[0])

    print "PID: ", pid_list

    for pid in pid_list:
        os.kill(int(pid), signal.SIGKILL)	# kill all process

    return True


def set_system_time(dt):

    str_time = dt.strftime("%Y-%m-%d %H:%M")
    print str_time

    # sample : timedatectl set-time YYYY-MM-DD HH:MM:SS
    os.system("date --set='" + str_time + "'")

    return True


if __name__ == '__main__':

    print "Starting command parser."
    ctrl = IniacCMDParser()

    while True:
        pending_cmd = ctrl.get_cmd_from_db()
        if pending_cmd:
            print "pending cmd : ", pending_cmd

            _id = pending_cmd[0]
            ctrl.excute_cmd(pending_cmd[1])

            if pending_cmd[1] == 'dev_id-all__factoryrst':
                print "Rebooting RPi now."
                os.system('sudo reboot')
            else:
                ctrl.delete_row(_id)

        time.sleep(5)       # the interval of executing request is 5 sec











