from iniac_event_handler import IniacEventHandler
import pyudev


class IniacMonitor(IniacEventHandler):

    context = None
    monitor = None

    def __init__(self):
        IniacEventHandler.__init__(self)
        self.context = pyudev.Context()
        self.monitor = pyudev.Monitor.from_netlink(self.context)
        self.monitor.filter_by(subsystem='usb')

    def start_monitoring(self):
        self.monitor.start()

        for action, dev in self.monitor:
            dev_dict = dict(dev)
            vid = dev_dict.get('ID_VENDOR_ID')
            pid = dev_dict.get('ID_MODEL_ID')
            serial_num = dev_dict.get('ID_SERIAL_SHORT')
            if vid:
                print "Action : ", action
                str_vid = vid.encode('ascii', 'ignore')
                str_pid = pid.encode('ascii', 'ignore')
                print "Vendor ID : ", str_vid
                print "Product ID : ", str_pid
                print "Serial Number: ", serial_num
                if str_vid == "0403" and str_pid == "6015":
                    if action == 'add':
                        print 'New device found, Serial Number is %s' % serial_num
                        self.add_log('info', 'New device found, Serial Number is %s' % serial_num)
                        self.add_new_device(serial_num)
                    else:
                        print 'Device is disconnected, Serial Number is %s' % serial_num
                        self.add_log('info', 'Device is disconnected, Serial Number is %s' % serial_num)
                        self.query_parser(serial_num, 'update_device_status_by_sn', False)

if __name__ == '__main__':
    ctrl = IniacMonitor()
    print "Monitoring is started"
    ctrl.start_monitoring()

